<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Local plugin "Self cohort membership"
 *
 * @package   local_selfcohort
 * @copyright 2020 Daniel Neis Araujo <daniel@adapta.online>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use local_selfcohort\action_result;
use local_selfcohort\cohorts_manager;
use local_selfcohort\local\table\register;
use local_selfcohort\membership_manager;

require_once('../../config.php');
require_once($CFG->dirroot . '/cohort/lib.php');

$page = optional_param('page', 0, PARAM_INT);
$cohortid = optional_param('cohortid', 0, PARAM_INT);
$action = optional_param('action', '', PARAM_ALPHA);

$url = new moodle_url('/local/selfcohort/register.php');
if ($page) {
    $url->param('page', $page);
}

require_login();

$PAGE->set_context(context_system::instance());
$PAGE->set_url($url);
$PAGE->set_title($SITE->fullname);
$PAGE->set_heading($SITE->fullname);

if (!empty($cohortid) && confirm_sesskey()) {
    $manager = new cohorts_manager();

    switch ($action) {
        case 'add':
            if (!$manager->require_confirm($cohortid)) {
                $result = $manager->add_to_cohort($cohortid, $USER->id);
            }
            break;
        case 'request':
            $approval = new membership_manager();
            $result = $approval->request_membership($cohortid, $USER->id);
            break;
        case 'remove':
            $result = $manager->remove_from_cohort($cohortid, $USER->id);
            break;
        default:
            $result = new action_result(false, get_string('incorrectaction', 'local_selfcohort'));
    }

    redirect($url, $result->get_message(), null, $result->get_messagetype());
}

$register = new register('local_selfcohort_register', $url);

echo $OUTPUT->header();
echo \html_writer::tag('div', get_string('registerintro', 'local_selfcohort'), ['id' => 'intro', 'class' => 'box generalbox']);
$register->out($register->pagesize, true);
echo $OUTPUT->footer();
