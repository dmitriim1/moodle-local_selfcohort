<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

namespace local_selfcohort;

use advanced_testcase;
use context_system;
use required_capability_exception;

/**
 * Tests for membership manager.
 *
 * @package    local_selfcohort
 * @author     Dmitrii Metelkin <dmitriim@catalyst-au.net>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 * @covers \local_selfcohort\capability_manager
 */
class capability_manager_test extends advanced_testcase {

    /**
     * Test can manage check.
     */
    public function test_can_manage() {
        $this->resetAfterTest();

        $context = context_system::instance();
        $user = $this->getDataGenerator()->create_user();

        $this->setUser($user);
        $this->assertFalse(capability_manager::can_manage($context));

        $roleid = $this->getDataGenerator()->create_role();
        $this->getDataGenerator()->create_role_capability(
            $roleid,
            [capability_manager::CAPABILITY_MANAGE => 'allow'],
            $context
        );

        $this->getDataGenerator()->role_assign($roleid, $user->id);

        $this->assertTrue(capability_manager::can_manage($context));
    }

    /**
     * Test can approve check.
     */
    public function test_can_can_approve_membership() {
        $this->resetAfterTest();

        $context = context_system::instance();
        $user = $this->getDataGenerator()->create_user();

        $this->setUser($user);
        $this->assertFalse(capability_manager::can_approve_membership($context));

        $roleid = $this->getDataGenerator()->create_role();
        $this->getDataGenerator()->create_role_capability(
            $roleid,
            [capability_manager::CAPABILITY_APPROVE => 'allow'],
            $context
        );

        $this->getDataGenerator()->role_assign($roleid, $user->id);

        $this->assertTrue(capability_manager::can_approve_membership($context));
    }

    /**
     * Test validation for can manage.
     */
    public function test_validate_can_manage() {
        $this->resetAfterTest();

        $context = context_system::instance();
        $user = $this->getDataGenerator()->create_user();

        $this->setUser($user);

        $this->expectException(required_capability_exception::class);
        $this->expectExceptionMessage(
            'Sorry, but you do not currently have permissions to do that (Create, delete and move cohorts).'
        );
        capability_manager::validate_can_manage($context);
    }

    /**
     * Test validation for can approve.
     */
    public function test_validate_can_approve_membership() {
        $this->resetAfterTest();

        $context = context_system::instance();
        $user = $this->getDataGenerator()->create_user();

        $this->setUser($user);

        $this->expectException(required_capability_exception::class);
        $this->expectExceptionMessage(
            'Sorry, but you do not currently have permissions to do that (Approve membership requests).'
        );
        capability_manager::validate_can_approve_membership($context);
    }
}
