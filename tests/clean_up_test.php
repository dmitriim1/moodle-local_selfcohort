<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

namespace local_selfcohort;

use advanced_testcase;

/**
 * Tests for cleaning up functionality.
 *
 * @package    local_selfcohort
 * @author     Dmitrii Metelkin <dmitriim@catalyst-au.net>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class clean_up_test extends advanced_testcase {

    /**
     * Test clean up after a cohort is deleted.
     *
     * @covers \local_selfcohort\observer::cohort_deleted
     */
    public function test_cohort_deleted() {
        $this->resetAfterTest();

        $cohort1 = $this->getDataGenerator()->create_cohort();
        $cohort2 = $this->getDataGenerator()->create_cohort();
        $cohort3 = $this->getDataGenerator()->create_cohort();

        $this->assertCount(0, cohort_settings::get_records());

        $settings1 = new cohort_settings();
        $settings1->set('cohortid', $cohort1->id);
        $settings1->save();

        $settings2 = new cohort_settings();
        $settings2->set('cohortid', $cohort2->id);
        $settings2->save();

        $settings3 = new cohort_settings();
        $settings3->set('cohortid', $cohort3->id);
        $settings3->save();

        $this->assertCount(3, cohort_settings::get_records());
        $this->assertCount(1, cohort_settings::get_records(['cohortid' => $cohort1->id]));
        $this->assertCount(1, cohort_settings::get_records(['cohortid' => $cohort2->id]));
        $this->assertCount(1, cohort_settings::get_records(['cohortid' => $cohort3->id]));

        cohort_delete_cohort($cohort1);
        $this->assertCount(2, cohort_settings::get_records());
        $this->assertCount(0, cohort_settings::get_records(['cohortid' => $cohort1->id]));
        $this->assertCount(1, cohort_settings::get_records(['cohortid' => $cohort2->id]));
        $this->assertCount(1, cohort_settings::get_records(['cohortid' => $cohort3->id]));

        cohort_delete_cohort($cohort2);
        $this->assertCount(1, cohort_settings::get_records());
        $this->assertCount(0, cohort_settings::get_records(['cohortid' => $cohort1->id]));
        $this->assertCount(0, cohort_settings::get_records(['cohortid' => $cohort2->id]));
        $this->assertCount(1, cohort_settings::get_records(['cohortid' => $cohort3->id]));

        cohort_delete_cohort($cohort3);
        $this->assertCount(0, cohort_settings::get_records());
        $this->assertCount(0, cohort_settings::get_records(['cohortid' => $cohort1->id]));
        $this->assertCount(0, cohort_settings::get_records(['cohortid' => $cohort2->id]));
        $this->assertCount(0, cohort_settings::get_records(['cohortid' => $cohort3->id]));
    }
}
