<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

namespace local_selfcohort;

use advanced_testcase;
use local_selfcohort\event\membership_request_approved;
use local_selfcohort\event\membership_request_declined;
use local_selfcohort\event\membership_requested;

/**
 * Tests for membership manager.
 *
 * @package    local_selfcohort
 * @author     Dmitrii Metelkin <dmitriim@catalyst-au.net>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 * @covers \local_selfcohort\membership_manager
 * @covers \local_selfcohort\action_result;
 */
class membership_manager_test extends advanced_testcase {

    /**
     * Test is confirm requested.
     */
    public function test_is_confirm_requested() {
        $this->resetAfterTest();

        $user = $this->getDataGenerator()->create_user();

        $cohort1 = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $cohort2 = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);

        $confirm = new cohort_confirm();
        $confirm->set('userid', $user->id);
        $confirm->set('cohortid', $cohort1->id);
        $confirm->save();

        $manager = new membership_manager();
        $this->assertTrue($manager->is_membership_requested($cohort1->id, $user->id));
        $this->assertFalse($manager->is_membership_requested($cohort2->id, $user->id));
        $this->assertFalse($manager->is_membership_requested($cohort1->id, 7777));
        $this->assertFalse($manager->is_membership_requested(7777, $user->id));
    }

    /**
     * Test get user roles for approving capability.
     */
    public function test_get_user_approval_roles() {
        $this->resetAfterTest();

        $user1 = $this->getDataGenerator()->create_user();
        $user2 = $this->getDataGenerator()->create_user();
        $user3 = $this->getDataGenerator()->create_user();

        $context = \context_system::instance();
        $roleid1 = $this->getDataGenerator()->create_role();
        $this->getDataGenerator()->create_role_capability(
            $roleid1,
            [capability_manager::CAPABILITY_APPROVE => 'allow'],
            $context
        );

        $roleid2 = $this->getDataGenerator()->create_role();
        $this->getDataGenerator()->create_role_capability(
            $roleid2,
            [capability_manager::CAPABILITY_APPROVE => 'allow'],
            $context
        );

        set_config('approvalroles', "$roleid1,$roleid2", 'local_selfcohort');

        $this->getDataGenerator()->role_assign($roleid1, $user1->id);
        $this->getDataGenerator()->role_assign($roleid2, $user1->id);
        $this->getDataGenerator()->role_assign($roleid1, $user2->id);

        $manager = new membership_manager();

        $this->setAdminUser();
        $this->assertEmpty($manager->get_user_approval_roles($context));

        $this->setUser($user1);
        $this->assertEquals([$roleid1, $roleid2], $manager->get_user_approval_roles($context));

        $this->setUser($user2);
        $this->assertEquals([$roleid1], $manager->get_user_approval_roles($context));

        $this->setUser($user3);
        $this->assertEmpty($manager->get_user_approval_roles($context));
    }

    /**
     * Test getting all configured approval roles.
     */
    public function test_get_approval_roles() {
        global $DB;

        $this->resetAfterTest();

        $context = \context_system::instance();

        $roleid1 = $this->getDataGenerator()->create_role();
        $role1name = role_get_name($DB->get_record('role', ['id' => $roleid1]));
        $this->getDataGenerator()->create_role_capability(
            $roleid1,
            [capability_manager::CAPABILITY_APPROVE => 'allow'],
            $context
        );

        $roleid2 = $this->getDataGenerator()->create_role();
        $role2name = role_get_name($DB->get_record('role', ['id' => $roleid2]));

        $roleid3 = $this->getDataGenerator()->create_role();
        $role3name = role_get_name($DB->get_record('role', ['id' => $roleid3]));

        $this->getDataGenerator()->create_role_capability(
            $roleid3,
            [capability_manager::CAPABILITY_APPROVE => 'allow'],
            $context
        );

        set_config('approvalroles', "$roleid1,$roleid2,$roleid3", 'local_selfcohort');

        $manager = new membership_manager();
        $this->assertEquals([$roleid1 => $role1name, $roleid3 => $role3name], $manager->get_approval_roles());
    }

    /**
     * Test can check if admin approval is required.
     */
    public function test_is_admin_approval_required() {
        $this->resetAfterTest();

        $context = \context_system::instance();

        $roleid1 = $this->getDataGenerator()->create_role();
        $this->getDataGenerator()->create_role_capability(
            $roleid1,
            [capability_manager::CAPABILITY_APPROVE => 'allow'],
            $context
        );

        set_config('enableroleapproval', 0, 'local_selfcohort');
        set_config('approvalroles', "", 'local_selfcohort');
        set_config('adminfinalapprove', 0, 'local_selfcohort');

        $manager = new membership_manager();
        $this->assertEmpty($manager->is_admin_approval_required());

        set_config('enableroleapproval', 1, 'local_selfcohort');
        set_config('approvalroles', "", 'local_selfcohort');
        set_config('adminfinalapprove', 0, 'local_selfcohort');

        $manager = new membership_manager();
        $this->assertEmpty($manager->is_admin_approval_required());

        set_config('enableroleapproval', 1, 'local_selfcohort');
        set_config('approvalroles', "$roleid1", 'local_selfcohort');
        set_config('adminfinalapprove', 0, 'local_selfcohort');

        $manager = new membership_manager();
        $this->assertEmpty($manager->is_admin_approval_required());

        set_config('enableroleapproval', 1, 'local_selfcohort');
        set_config('approvalroles', "$roleid1", 'local_selfcohort');
        set_config('adminfinalapprove', 1, 'local_selfcohort');

        $manager = new membership_manager();
        $this->assertTrue($manager->is_admin_approval_required());
    }

    /**
     * Test can check if role approval is enabled.
     */
    public function test_is_role_approval_enabled() {
        $this->resetAfterTest();

        $context = \context_system::instance();

        $roleid1 = $this->getDataGenerator()->create_role();
        $this->getDataGenerator()->create_role_capability(
            $roleid1,
            [capability_manager::CAPABILITY_APPROVE => 'allow'],
            $context
        );

        $roleid2 = $this->getDataGenerator()->create_role();

        $manager = new membership_manager();
        $this->assertFalse($manager->is_role_approval_enabled());

        set_config('enableroleapproval', 1, 'local_selfcohort');
        set_config('approvalroles', "", 'local_selfcohort');

        $manager = new membership_manager();
        $this->assertFalse($manager->is_role_approval_enabled());

        set_config('enableroleapproval', 1, 'local_selfcohort');
        set_config('approvalroles', "$roleid2", 'local_selfcohort');

        $manager = new membership_manager();
        $this->assertFalse($manager->is_role_approval_enabled());

        set_config('enableroleapproval', 0, 'local_selfcohort');
        set_config('approvalroles', "$roleid1", 'local_selfcohort');

        $manager = new membership_manager();
        $this->assertFalse($manager->is_role_approval_enabled());

        set_config('enableroleapproval', 1, 'local_selfcohort');
        set_config('approvalroles', "$roleid1", 'local_selfcohort');

        $manager = new membership_manager();
        $this->assertTrue($manager->is_role_approval_enabled());
    }

    /**
     * Test we can check if admin has approved a given confirmation request.
     */
    public function test_has_admin_approved() {
        $this->resetAfterTest();

        $manager = new membership_manager();

        $this->assertFalse($manager->has_admin_approved(7777));

        $approve = new cohort_approve();
        $approve->set('confirmid', 7777);
        $approve->set('roleid', membership_manager::ADMIN_ROLE_ID);
        $approve->set('status', cohort_approve::STATUS_NEW);
        $approve->save();
        $this->assertFalse($manager->has_admin_approved(7777));

        $approve->set('status', cohort_approve::STATUS_DECLINED);
        $approve->save();
        $this->assertFalse($manager->has_admin_approved(7777));

        $approve->set('status', cohort_approve::STATUS_APPROVED);
        $approve->save();
        $this->assertTrue($manager->has_admin_approved(7777));

        $approve->set('confirmid', 11111);
        $approve->save();
        $this->assertFalse($manager->has_admin_approved(7777));
    }

    /**
     * Test we can check if a given role has approved a given confirmation request.
     */
    public function test_has_role_approved() {
        $this->resetAfterTest();

        $manager = new membership_manager();

        $this->assertFalse($manager->has_role_approved(7777, 5));

        $approve = new cohort_approve();
        $approve->set('confirmid', 7777);
        $approve->set('roleid', 5);
        $approve->set('status', cohort_approve::STATUS_NEW);
        $approve->save();
        $this->assertFalse($manager->has_role_approved(7777, 5));

        $approve->set('status', cohort_approve::STATUS_DECLINED);
        $approve->save();
        $this->assertFalse($manager->has_role_approved(7777, 5));

        $approve->set('status', cohort_approve::STATUS_APPROVED);
        $approve->save();
        $this->assertTrue($manager->has_role_approved(7777, 5));

        $approve->set('confirmid', 11111);
        $approve->save();
        $this->assertFalse($manager->has_role_approved(7777, 5));
    }

    /**
     * Test requesting a membership.
     */
    public function test_request_membership_without_enableroleapproval() {
        $this->resetAfterTest();

        $context = \context_system::instance();
        $user = $this->getDataGenerator()->create_user();
        $usermanager = $this->getDataGenerator()->create_user();
        $roleid = $this->getDataGenerator()->create_role();
        $this->getDataGenerator()->create_role_capability(
            $roleid,
            [capability_manager::CAPABILITY_RECEIVE_NOTIFICATION => 'allow'],
            $context
        );

        // Enable admin notification.
        set_config('notifyadmins', 1, 'local_selfcohort');
        $extraadminnotifications = count(get_admins());

        $this->getDataGenerator()->role_assign($roleid, $usermanager->id);

        $recipients = get_users_by_capability($context, capability_manager::CAPABILITY_RECEIVE_NOTIFICATION);
        $this->assertCount(1, $recipients);

        $cohort1 = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $cohort2 = $this->getDataGenerator()->create_cohort(['component' => '']);

        $this->assertCount(0, cohort_confirm::get_records(['userid' => $user->id]));

        $manager = new membership_manager();

        $eventsink = $this->redirectEvents();
        $messagesink = $this->redirectMessages();

        $result = $manager->request_membership($cohort1->id, $user->id);
        $this->assertTrue($result->get_result());
        $this->assertSame(get_string('confirmrequested', 'local_selfcohort'), $result->get_message());
        $this->assertCount(1, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort1->id]));
        $this->assertCount(0, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort2->id]));

        $this->assertCount(0, cohort_approve::get_records());

        $messages = $messagesink->get_messages();
        $this->assertCount(1 + $extraadminnotifications, $messages);
        $message = reset($messages);
        $this->assertSame($usermanager->id, $message->useridto);
        $this->assertSame(get_string('requestsubject', 'local_selfcohort'), $message->subject);
        $fullmessage = get_string('requestfullmessage', 'local_selfcohort', (object)[
            'userid' => $user->id,
            'userfullname' => fullname($user),
            'cohortid' => $cohort1->id,
            'cohortname' => $cohort1->name,
        ]);
        $this->assertSame($fullmessage, $message->fullmessage);
        $messagesink->clear();

        $result = $manager->request_membership($cohort2->id, $user->id);
        $this->assertFalse($result->get_result());

        $messages = $messagesink->get_messages();
        $this->assertCount(0, $messages);
        $messagesink->clear();

        $this->assertSame(get_string('invalidcohort', 'local_selfcohort'), $result->get_message());
        $this->assertCount(1, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort1->id]));
        $this->assertCount(0, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort2->id]));

        $this->assertCount(0, cohort_approve::get_records());

        // Request already exists we shouldn't create another one.
        $result = $manager->request_membership($cohort1->id, $user->id);

        $this->assertTrue($result->get_result());

        $messages = $messagesink->get_messages();
        $this->assertCount(0, $messages);
        $messagesink->clear();

        $this->assertSame(get_string('confirmrequested', 'local_selfcohort'), $result->get_message());
        $this->assertCount(1, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort1->id]));
        $this->assertCount(0, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort2->id]));

        // Test events.
        $events = array_filter($eventsink->get_events(), function ($event) {
            return $event instanceof membership_requested;
        });

        $this->assertCount(1, $events);
        $this->assertEquals($user->id, reset($events)->userid);
        $this->assertEquals($cohort1->id, reset($events)->other['cohortid']);
        $eventsink->clear();
    }

    /**
     * Test approving membership request without enabling roles approval.
     */
    public function test_approve_membership_request_without_enableroleapproval() {
        $this->resetAfterTest();

        $user = $this->getDataGenerator()->create_user();

        $cohort1 = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $cohort2 = $this->getDataGenerator()->create_cohort(['component' => '']);
        $cohort3 = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);

        $eventsink = $this->redirectEvents();
        $messagesink = $this->redirectMessages();

        $manager = new membership_manager();

        $this->assertFalse(cohort_is_member($cohort1->id, $user->id));
        $this->assertFalse(cohort_is_member($cohort2->id, $user->id));
        $this->assertFalse(cohort_is_member($cohort3->id, $user->id));

        $result = $manager->approve_membership_request($cohort1->id, $user->id);
        $this->assertFalse($result->get_result());
        $this->assertSame(get_string('invalidconfirm', 'local_selfcohort'), $result->get_message());

        $result = $manager->approve_membership_request($cohort2->id, $user->id);
        $this->assertFalse($result->get_result());
        $this->assertSame(get_string('invalidconfirm', 'local_selfcohort'), $result->get_message());

        $result = $manager->approve_membership_request($cohort3->id, $user->id);
        $this->assertFalse($result->get_result());
        $this->assertSame(get_string('invalidconfirm', 'local_selfcohort'), $result->get_message());

        $messages = $messagesink->get_messages();
        $this->assertCount(0, $messages);
        $messagesink->clear();

        $confirm = new cohort_confirm();
        $confirm->set('userid', $user->id);
        $confirm->set('cohortid', $cohort1->id);
        $confirm->save();

        $confirm = new cohort_confirm();
        $confirm->set('userid', $user->id);
        $confirm->set('cohortid', $cohort2->id);
        $confirm->save();

        $confirm = new cohort_confirm();
        $confirm->set('userid', $user->id);
        $confirm->set('cohortid', $cohort3->id);
        $confirm->save();

        $settings = new cohort_settings();
        $settings->set('cohortid', $cohort3->id);
        $settings->set('maxmembers', -1); // No members can be added.
        $settings->save();

        $this->assertCount(3, cohort_confirm::get_records(['userid' => $user->id]));
        $this->assertCount(1, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort1->id]));
        $this->assertCount(1, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort2->id]));
        $this->assertCount(1, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort3->id]));

        $manager = new membership_manager();

        $result = $manager->approve_membership_request($cohort1->id, $user->id);
        $this->assertTrue($result->get_result());
        $this->assertSame(get_string('confirmapproved', 'local_selfcohort'), $result->get_message());
        $this->assertTrue(cohort_is_member($cohort1->id, $user->id));

        $messages = $messagesink->get_messages();
        $this->assertCount(1, $messages);
        $message = reset($messages);
        $this->assertSame($user->id, $message->useridto);
        $this->assertSame(get_string('requestsubjectapprove', 'local_selfcohort'), $message->subject);

        $fullmessage = get_string('requestfullmessageapprove', 'local_selfcohort', (object)[
            'userid' => $user->id,
            'userfullname' => fullname($user),
            'cohortid' => $cohort1->id,
            'cohortname' => $cohort1->name,
        ]);
        $this->assertSame($fullmessage, $message->fullmessage);
        $messagesink->clear();

        $result = $manager->approve_membership_request($cohort2->id, $user->id);
        $this->assertFalse($result->get_result());
        $this->assertSame(get_string('invalidcohort', 'local_selfcohort'), $result->get_message());
        $this->assertFalse(cohort_is_member($cohort2->id, $user->id));

        $messages = $messagesink->get_messages();
        $this->assertCount(0, $messages);
        $messagesink->clear();

        $result = $manager->approve_membership_request($cohort3->id, $user->id);
        $this->assertFalse($result->get_result());
        $this->assertSame(get_string('cohortfull', 'local_selfcohort'), $result->get_message());
        $this->assertFalse(cohort_is_member($cohort3->id, $user->id));

        $messages = $messagesink->get_messages();
        $this->assertCount(0, $messages);
        $messagesink->clear();

        $this->assertCount(2, cohort_confirm::get_records(['userid' => $user->id]));
        $this->assertCount(0, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort1->id]));
        $this->assertCount(1, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort2->id]));
        $this->assertCount(1, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort3->id]));

        // Test events.
        $events = array_filter($eventsink->get_events(), function ($event) {
            return $event instanceof membership_request_approved;
        });

        $this->assertCount(1, $events);
        $this->assertEquals($user->id, reset($events)->other['requester']);
        $this->assertEquals($cohort1->id, reset($events)->other['cohortid']);
        $eventsink->clear();
    }

    /**
     * Test declining membership request.
     */
    public function test_decline_membership_request_without_enableroleapproval() {
        $this->resetAfterTest();

        $user = $this->getDataGenerator()->create_user();

        $cohort1 = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $cohort2 = $this->getDataGenerator()->create_cohort(['component' => '']);
        $cohort3 = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);

        $confirm = new cohort_confirm();
        $confirm->set('userid', $user->id);
        $confirm->set('cohortid', $cohort1->id);
        $confirm->save();

        $this->assertCount(1, cohort_confirm::get_records(['userid' => $user->id]));
        $this->assertCount(1, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort1->id]));
        $this->assertCount(0, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort2->id]));
        $this->assertCount(0, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort3->id]));

        $eventsink = $this->redirectEvents();
        $messagesink = $this->redirectMessages();

        $manager = new membership_manager();
        $this->assertFalse(cohort_is_member($cohort1->id, $user->id));
        $this->assertFalse(cohort_is_member($cohort2->id, $user->id));
        $this->assertFalse(cohort_is_member($cohort3->id, $user->id));

        $result = $manager->decline_membership_request($cohort1->id, $user->id);
        $this->assertTrue($result->get_result());
        $this->assertSame(get_string('confirmdeclined', 'local_selfcohort'), $result->get_message());

        $messages = $messagesink->get_messages();
        $this->assertCount(1, $messages);
        $message = reset($messages);
        $this->assertSame($user->id, $message->useridto);
        $this->assertSame(get_string('requestsubjectdecline', 'local_selfcohort'), $message->subject);

        $fullmessage = get_string('requestfullmessagedecline', 'local_selfcohort', (object)[
            'userid' => $user->id,
            'userfullname' => fullname($user),
            'cohortid' => $cohort1->id,
            'cohortname' => $cohort1->name,
        ]);
        $this->assertSame($fullmessage, $message->fullmessage);
        $messagesink->clear();

        $result = $manager->decline_membership_request($cohort2->id, $user->id);
        $this->assertFalse($result->get_result());
        $this->assertSame(get_string('invalidcohort', 'local_selfcohort'), $result->get_message());
        $this->assertFalse(cohort_is_member($cohort2->id, $user->id));
        $messages = $messagesink->get_messages();
        $this->assertCount(0, $messages);
        $messagesink->clear();

        $result = $manager->decline_membership_request($cohort3->id, $user->id);
        $this->assertFalse($result->get_result());
        $this->assertSame(get_string('invalidconfirm', 'local_selfcohort'), $result->get_message());
        $this->assertFalse(cohort_is_member($cohort3->id, $user->id));
        $messages = $messagesink->get_messages();
        $this->assertCount(0, $messages);
        $messagesink->clear();

        $this->assertFalse(cohort_is_member($cohort1->id, $user->id));
        $this->assertFalse(cohort_is_member($cohort2->id, $user->id));
        $this->assertFalse(cohort_is_member($cohort3->id, $user->id));

        $this->assertCount(0, cohort_confirm::get_records(['userid' => $user->id]));

        // Test events.
        $events = array_filter($eventsink->get_events(), function ($event) {
            return $event instanceof membership_request_declined;
        });

        $this->assertCount(1, $events);
        $this->assertEquals($user->id, reset($events)->other['requester']);
        $this->assertEquals($cohort1->id, reset($events)->other['cohortid']);
        $eventsink->clear();
    }

    /**
     * Test requesting a membership with enabled roles approval.
     */
    public function test_request_membership_with_enableroleapproval() {
        $this->resetAfterTest();

        $context = \context_system::instance();
        $user = $this->getDataGenerator()->create_user();
        $usermanager = $this->getDataGenerator()->create_user();
        $roleid = $this->getDataGenerator()->create_role();
        $this->getDataGenerator()->create_role_capability(
            $roleid,
            [
                capability_manager::CAPABILITY_RECEIVE_NOTIFICATION => 'allow',
                capability_manager::CAPABILITY_APPROVE => 'allow',
            ],
            $context
        );

        $roleid2 = $this->getDataGenerator()->create_role();
        $this->getDataGenerator()->create_role_capability(
            $roleid2,
            [capability_manager::CAPABILITY_APPROVE => 'allow'],
            $context
        );

        set_config('enableroleapproval', 1, 'local_selfcohort');
        set_config('approvalroles', "$roleid,$roleid2,5", 'local_selfcohort');
        set_config('approvalrule', membership_manager::APPROVAL_RULE_ALL, 'local_selfcohort');
        set_config('adminfinalapprove', 1, 'local_selfcohort');

        $this->getDataGenerator()->role_assign($roleid, $usermanager->id);

        $recipients = get_users_by_capability($context, capability_manager::CAPABILITY_RECEIVE_NOTIFICATION);
        $this->assertCount(1, $recipients);

        $cohort1 = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $cohort2 = $this->getDataGenerator()->create_cohort(['component' => '']);

        $this->assertCount(0, cohort_confirm::get_records(['userid' => $user->id]));

        $manager = new membership_manager();

        $eventsink = $this->redirectEvents();
        $messagesink = $this->redirectMessages();

        $result = $manager->request_membership($cohort1->id, $user->id);
        $this->assertTrue($result->get_result());
        $this->assertSame(get_string('confirmrequested', 'local_selfcohort'), $result->get_message());
        $this->assertCount(1, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort1->id]));
        $this->assertCount(0, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort2->id]));

        // Should create three records - one for admin and two for roles with local/selfcohort:approverequestmembership.
        // Role with id 5 should be ignored as it doesn't have required capability.
        $this->assertCount(3, cohort_approve::get_records());
        $this->assertCount(3, cohort_approve::get_records(['status' => cohort_approve::STATUS_NEW]));
        $this->assertCount(1, cohort_approve::get_records(['roleid' => $roleid]));
        $this->assertCount(1, cohort_approve::get_records(['roleid' => $roleid2]));

        $this->assertCount(1, cohort_approve::get_records(['roleid' => membership_manager::ADMIN_ROLE_ID]));

        $messages = $messagesink->get_messages();
        $this->assertCount(1, $messages);
        $message = reset($messages);
        $this->assertSame($usermanager->id, $message->useridto);
        $this->assertSame(get_string('requestsubject', 'local_selfcohort'), $message->subject);
        $fullmessage = get_string('requestfullmessage', 'local_selfcohort', (object)[
            'userid' => $user->id,
            'userfullname' => fullname($user),
            'cohortid' => $cohort1->id,
            'cohortname' => $cohort1->name,
        ]);
        $this->assertSame($fullmessage, $message->fullmessage);
        $messagesink->clear();

        $result = $manager->request_membership($cohort2->id, $user->id);
        $this->assertFalse($result->get_result());

        $messages = $messagesink->get_messages();
        $this->assertCount(0, $messages);
        $messagesink->clear();

        $this->assertSame(get_string('invalidcohort', 'local_selfcohort'), $result->get_message());
        $this->assertCount(1, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort1->id]));
        $this->assertCount(0, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort2->id]));

        // Request already exists we shouldn't create another one.
        $result = $manager->request_membership($cohort1->id, $user->id);

        $this->assertTrue($result->get_result());

        $messages = $messagesink->get_messages();
        $this->assertCount(0, $messages);
        $messagesink->clear();

        $this->assertSame(get_string('confirmrequested', 'local_selfcohort'), $result->get_message());
        $this->assertCount(1, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort1->id]));
        $this->assertCount(0, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort2->id]));

        $this->assertCount(3, cohort_approve::get_records());
        $this->assertCount(3, cohort_approve::get_records(['status' => cohort_approve::STATUS_NEW]));
        $this->assertCount(1, cohort_approve::get_records(['roleid' => $roleid]));
        $this->assertCount(1, cohort_approve::get_records(['roleid' => $roleid2]));

        // Test events.
        $events = array_filter($eventsink->get_events(), function ($event) {
            return $event instanceof membership_requested;
        });

        $this->assertCount(1, $events);
        $this->assertEquals($user->id, reset($events)->userid);
        $this->assertEquals($cohort1->id, reset($events)->other['cohortid']);
        $eventsink->clear();
    }

    /**
     * Test requesting a membership with enabled roles approval, but without configured roles.
     */
    public function test_request_membership_with_enableroleapproval_but_no_roles() {
        $this->resetAfterTest();

        $context = \context_system::instance();
        $user = $this->getDataGenerator()->create_user();
        $usermanager = $this->getDataGenerator()->create_user();
        $roleid = $this->getDataGenerator()->create_role();
        $this->getDataGenerator()->create_role_capability(
            $roleid,
            [
                capability_manager::CAPABILITY_APPROVE => 'allow',
                capability_manager::CAPABILITY_RECEIVE_NOTIFICATION => 'allow',
            ],
            $context
        );

        set_config('enableroleapproval', 1, 'local_selfcohort');
        set_config('approvalroles', '', 'local_selfcohort');
        set_config('approvalrule', membership_manager::APPROVAL_RULE_ALL, 'local_selfcohort');
        set_config('adminfinalapprove', 1, 'local_selfcohort');

        $this->getDataGenerator()->role_assign($roleid, $usermanager->id);

        $recipients = get_users_by_capability($context, capability_manager::CAPABILITY_RECEIVE_NOTIFICATION);
        $this->assertCount(1, $recipients);

        $cohort1 = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $cohort2 = $this->getDataGenerator()->create_cohort(['component' => '']);

        $this->assertCount(0, cohort_confirm::get_records(['userid' => $user->id]));

        $manager = new membership_manager();

        $result = $manager->request_membership($cohort1->id, $user->id);
        $this->assertTrue($result->get_result());
        $this->assertSame(get_string('confirmrequested', 'local_selfcohort'), $result->get_message());
        $this->assertCount(1, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort1->id]));
        $this->assertCount(0, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort2->id]));

        // Shouldn't create any approvals as no roles.
        $this->assertCount(0, cohort_approve::get_records());

        $result = $manager->request_membership($cohort2->id, $user->id);
        $this->assertFalse($result->get_result());

        $this->assertSame(get_string('invalidcohort', 'local_selfcohort'), $result->get_message());
        $this->assertCount(1, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort1->id]));
        $this->assertCount(0, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort2->id]));

        // Request already exists we shouldn't create another one.
        $result = $manager->request_membership($cohort1->id, $user->id);
        $this->assertTrue($result->get_result());

        $this->assertSame(get_string('confirmrequested', 'local_selfcohort'), $result->get_message());
        $this->assertCount(1, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort1->id]));
        $this->assertCount(0, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort2->id]));

        $this->assertCount(0, cohort_approve::get_records());
    }

    /**
     * Test requesting a membership with enabled roles approval without admin approval.
     */
    public function test_request_membership_with_enableroleapproval_without_admin() {
        $this->resetAfterTest();

        $context = \context_system::instance();
        $user = $this->getDataGenerator()->create_user();
        $usermanager = $this->getDataGenerator()->create_user();
        $roleid = $this->getDataGenerator()->create_role();
        $this->getDataGenerator()->create_role_capability(
            $roleid,
            [
                capability_manager::CAPABILITY_APPROVE => 'allow',
                capability_manager::CAPABILITY_RECEIVE_NOTIFICATION => 'allow',
            ],
            $context
        );

        set_config('enableroleapproval', 1, 'local_selfcohort');
        set_config('approvalroles', $roleid, 'local_selfcohort');
        set_config('approvalrule', membership_manager::APPROVAL_RULE_ALL, 'local_selfcohort');
        set_config('adminfinalapprove', 0, 'local_selfcohort');

        $this->getDataGenerator()->role_assign($roleid, $usermanager->id);

        $cohort1 = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $cohort2 = $this->getDataGenerator()->create_cohort(['component' => '']);

        $this->assertCount(0, cohort_confirm::get_records(['userid' => $user->id]));

        $manager = new membership_manager();

        $result = $manager->request_membership($cohort1->id, $user->id);
        $this->assertTrue($result->get_result());
        $this->assertSame(get_string('confirmrequested', 'local_selfcohort'), $result->get_message());
        $this->assertCount(1, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort1->id]));
        $this->assertCount(0, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort2->id]));

        // Should create 1 record for manager role only.
        $this->assertCount(1, cohort_approve::get_records());
        $this->assertCount(1, cohort_approve::get_records(['status' => cohort_approve::STATUS_NEW]));
        $this->assertCount(1, cohort_approve::get_records(['roleid' => $roleid]));
        $this->assertCount(0, cohort_approve::get_records(['roleid' => membership_manager::ADMIN_ROLE_ID]));

        $result = $manager->request_membership($cohort2->id, $user->id);
        $this->assertFalse($result->get_result());

        $this->assertSame(get_string('invalidcohort', 'local_selfcohort'), $result->get_message());
        $this->assertCount(1, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort1->id]));
        $this->assertCount(0, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort2->id]));

        // Request already exists we shouldn't create another one.
        $result = $manager->request_membership($cohort1->id, $user->id);
        $this->assertTrue($result->get_result());

        $this->assertSame(get_string('confirmrequested', 'local_selfcohort'), $result->get_message());
        $this->assertCount(1, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort1->id]));
        $this->assertCount(0, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort2->id]));

        // Should create 1 record for manager role only.
        $this->assertCount(1, cohort_approve::get_records());
        $this->assertCount(1, cohort_approve::get_records(['status' => cohort_approve::STATUS_NEW]));
        $this->assertCount(1, cohort_approve::get_records(['roleid' => $roleid]));
        $this->assertCount(0, cohort_approve::get_records(['roleid' => membership_manager::ADMIN_ROLE_ID]));
    }

    /**
     * Test exception thrown if declining membership request with roles approval enabled, but without a role provided.
     */
    public function test_decline_membership_request_without_role_id() {
        $this->resetAfterTest();

        $cohort = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $context = \context_system::instance();
        $roleid = $this->getDataGenerator()->create_role();
        $this->getDataGenerator()->create_role_capability(
            $roleid,
            [capability_manager::CAPABILITY_APPROVE => 'allow'],
            $context
        );

        set_config('enableroleapproval', 1, 'local_selfcohort');
        set_config('approvalroles', $roleid, 'local_selfcohort');

        $confirm = new cohort_confirm();
        $confirm->set('userid', 1);
        $confirm->set('cohortid', $cohort->id);
        $confirm->save();

        $this->expectException(\coding_exception::class);
        $this->expectExceptionMessage('Role ID is required during declining when roles approval feature is enabled.');
        $manager = new membership_manager();
        $manager->decline_membership_request($cohort->id, 1);
    }

    /**
     * Test declining membership request by admin when role approval is enabled.
     */
    public function test_decline_membership_request_with_enableroleapproval_by_admin() {
        $this->resetAfterTest();

        $user = $this->getDataGenerator()->create_user();

        $this->setAdminUser();

        $cohort = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $context = \context_system::instance();
        $roleid = $this->getDataGenerator()->create_role();
        $this->getDataGenerator()->create_role_capability(
            $roleid,
            [capability_manager::CAPABILITY_APPROVE => 'allow'],
            $context
        );

        set_config('enableroleapproval', 1, 'local_selfcohort');
        set_config('approvalroles', $roleid, 'local_selfcohort');

        $confirm = new cohort_confirm();
        $confirm->set('userid', $user->id);
        $confirm->set('cohortid', $cohort->id);
        $confirm->save();

        $this->assertCount(1, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort->id]));
        $this->assertFalse(cohort_is_member($cohort->id, $user->id));

        $manager = new membership_manager();

        // Admin can decline on behalf of any role.
        $result = $manager->decline_membership_request($cohort->id, $user->id, rand(0, 100));
        $this->assertTrue($result->get_result());
        $this->assertSame(get_string('confirmdeclined', 'local_selfcohort'), $result->get_message());

        $this->assertFalse(cohort_is_member($cohort->id, $user->id));
        $this->assertCount(0, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort->id]));
    }

    /**
     * Test declining membership request by a user without required role when role approval is enabled.
     */
    public function test_decline_membership_request_with_enableroleapproval_by_user_without_role() {
        $this->resetAfterTest();

        $user = $this->getDataGenerator()->create_user();

        $cohort = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $context = \context_system::instance();
        $roleid = $this->getDataGenerator()->create_role();
        $this->getDataGenerator()->create_role_capability(
            $roleid,
            [capability_manager::CAPABILITY_APPROVE => 'allow'],
            $context
        );

        set_config('enableroleapproval', 1, 'local_selfcohort');
        set_config('approvalroles', $roleid, 'local_selfcohort');

        $this->setUser($user);

        $confirm = new cohort_confirm();
        $confirm->set('userid', $user->id);
        $confirm->set('cohortid', $cohort->id);
        $confirm->save();

        $manager = new membership_manager();

        $this->expectException(\required_capability_exception::class);
        $this->expectExceptionMessage('Sorry, but you do not currently have permissions to do that (Approve membership requests)');

        $manager->decline_membership_request($cohort->id, $user->id, $roleid);
    }

    /**
     * Test declining membership request by a user with required role, but using a different role for approval.
     */
    public function test_decline_membership_request_with_enableroleapproval_by_user_with_role_but_using_different_role() {
        $this->resetAfterTest();

        $user = $this->getDataGenerator()->create_user();

        $cohort = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $context = \context_system::instance();
        $roleid1 = $this->getDataGenerator()->create_role();
        $this->getDataGenerator()->create_role_capability(
            $roleid1,
            [capability_manager::CAPABILITY_APPROVE => 'allow'],
            $context
        );

        $roleid2 = $this->getDataGenerator()->create_role();
        $this->getDataGenerator()->create_role_capability(
            $roleid2,
            [capability_manager::CAPABILITY_APPROVE => 'allow'],
            $context
        );

        set_config('enableroleapproval', 1, 'local_selfcohort');
        set_config('approvalroles', "$roleid1,$roleid2", 'local_selfcohort');

        $this->getDataGenerator()->role_assign($roleid1, $user->id);

        $this->setUser($user);

        $confirm = new cohort_confirm();
        $confirm->set('userid', $user->id);
        $confirm->set('cohortid', $cohort->id);
        $confirm->save();

        $manager = new membership_manager();

        $this->expectException(\required_capability_exception::class);
        $this->expectExceptionMessage('Sorry, but you do not currently have permissions to do that (Approve membership requests)');

        $manager->decline_membership_request($cohort->id, $user->id, $roleid2);
    }

    /**
     * Test declining membership request by a user with required role using a correct role for approval.
     */
    public function test_decline_membership_request_with_enableroleapproval_by_user_with_role_but_using_correct_role() {
        $this->resetAfterTest();

        $user = $this->getDataGenerator()->create_user();

        $cohort = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $context = \context_system::instance();
        $roleid = $this->getDataGenerator()->create_role();
        $this->getDataGenerator()->create_role_capability(
            $roleid,
            [capability_manager::CAPABILITY_APPROVE => 'allow'],
            $context
        );

        set_config('enableroleapproval', 1, 'local_selfcohort');
        set_config('approvalroles', "$roleid", 'local_selfcohort');

        $this->getDataGenerator()->role_assign($roleid, $user->id);

        $this->setUser($user);

        $confirm = new cohort_confirm();
        $confirm->set('userid', $user->id);
        $confirm->set('cohortid', $cohort->id);
        $confirm->save();

        $manager = new membership_manager();
        $result = $manager->decline_membership_request($cohort->id, $user->id, $roleid);

        $this->assertTrue($result->get_result());
        $this->assertSame(get_string('confirmdeclined', 'local_selfcohort'), $result->get_message());

        $this->assertFalse(cohort_is_member($cohort->id, $user->id));
        $this->assertCount(0, cohort_confirm::get_records(['userid' => $user->id, 'cohortid' => $cohort->id]));
    }

    /**
     * Test approving membership request by a user without required role when role approval is enabled.
     */
    public function test_approve_membership_request_with_enableroleapproval_by_user_without_role() {
        $this->resetAfterTest();

        $user = $this->getDataGenerator()->create_user();

        $cohort = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $context = \context_system::instance();
        $roleid = $this->getDataGenerator()->create_role();
        $this->getDataGenerator()->create_role_capability(
            $roleid,
            [capability_manager::CAPABILITY_APPROVE => 'allow'],
            $context
        );

        set_config('enableroleapproval', 1, 'local_selfcohort');
        set_config('approvalroles', $roleid, 'local_selfcohort');

        $this->setUser($user);

        $confirm = new cohort_confirm();
        $confirm->set('userid', $user->id);
        $confirm->set('cohortid', $cohort->id);
        $confirm->save();

        $manager = new membership_manager();

        $this->expectException(\required_capability_exception::class);
        $this->expectExceptionMessage('Sorry, but you do not currently have permissions to do that (Approve membership requests)');

        $manager->approve_membership_request($cohort->id, $user->id, $roleid);
    }

    /**
     * Test approving membership request by a user with required role, but using a different role for approval.
     */
    public function test_approve_membership_request_with_enableroleapproval_by_user_with_role_but_using_different_role() {
        $this->resetAfterTest();

        $user = $this->getDataGenerator()->create_user();

        $cohort = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $context = \context_system::instance();
        $roleid1 = $this->getDataGenerator()->create_role();
        $this->getDataGenerator()->create_role_capability(
            $roleid1,
            [capability_manager::CAPABILITY_APPROVE => 'allow'],
            $context
        );

        $roleid2 = $this->getDataGenerator()->create_role();
        $this->getDataGenerator()->create_role_capability(
            $roleid2,
            [capability_manager::CAPABILITY_APPROVE => 'allow'],
            $context
        );

        set_config('enableroleapproval', 1, 'local_selfcohort');
        set_config('approvalroles', "$roleid1,$roleid2", 'local_selfcohort');

        $this->getDataGenerator()->role_assign($roleid1, $user->id);

        $this->setUser($user);

        $confirm = new cohort_confirm();
        $confirm->set('userid', $user->id);
        $confirm->set('cohortid', $cohort->id);
        $confirm->save();

        $manager = new membership_manager();

        $this->expectException(\required_capability_exception::class);
        $this->expectExceptionMessage('Sorry, but you do not currently have permissions to do that (Approve membership requests)');

        $manager->approve_membership_request($cohort->id, $user->id, $roleid2);
    }

    /**
     * Test approving membership request without providing a role, when role approval is enabled.
     */
    public function test_approve_membership_request_with_enableroleapproval_calling_without_role() {
        $this->resetAfterTest();

        $user = $this->getDataGenerator()->create_user();

        $cohort = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $context = \context_system::instance();
        $roleid1 = $this->getDataGenerator()->create_role();
        $this->getDataGenerator()->create_role_capability(
            $roleid1,
            [capability_manager::CAPABILITY_APPROVE => 'allow'],
            $context
        );

        set_config('enableroleapproval', 1, 'local_selfcohort');
        set_config('approvalroles', "$roleid1", 'local_selfcohort');

        $this->getDataGenerator()->role_assign($roleid1, $user->id);
        $this->setUser($user);

        $confirm = new cohort_confirm();
        $confirm->set('userid', $user->id);
        $confirm->set('cohortid', $cohort->id);
        $confirm->save();

        $manager = new membership_manager();

        $this->expectException(\coding_exception::class);
        $this->expectExceptionMessage('Role ID is required during approval when roles approval feature is enabled.');

        $manager->approve_membership_request($cohort->id, $user->id);
    }

    /**
     * Test approving membership request with invalid approval request
     */
    public function test_approve_membership_request_with_enableroleapproval_invalid_approval_request() {
        $this->resetAfterTest();

        $user = $this->getDataGenerator()->create_user();

        $cohort = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $context = \context_system::instance();
        $roleid1 = $this->getDataGenerator()->create_role();
        $this->getDataGenerator()->create_role_capability(
            $roleid1,
            [capability_manager::CAPABILITY_APPROVE => 'allow'],
            $context
        );

        set_config('enableroleapproval', 1, 'local_selfcohort');
        set_config('approvalroles', "$roleid1", 'local_selfcohort');

        $this->getDataGenerator()->role_assign($roleid1, $user->id);
        $this->setUser($user);

        // Creating a confirmation bypassing creating approval requests for configured roles.
        $confirm = new cohort_confirm();
        $confirm->set('userid', $user->id);
        $confirm->set('cohortid', $cohort->id);
        $confirm->save();

        $manager = new membership_manager();

        $result = $manager->approve_membership_request($cohort->id, $user->id, $roleid1);
        $this->assertFalse($result->get_result());
        $this->assertSame(get_string('invalidapproval', 'local_selfcohort'), $result->get_message());
    }

    /**
     * Test approving membership request when any role can approve.
     */
    public function test_approve_membership_request_with_enableroleapproval_rule_any_role() {
        $this->resetAfterTest();

        $user = $this->getDataGenerator()->create_user();

        $cohort = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $context = \context_system::instance();

        $roleid1 = $this->getDataGenerator()->create_role();
        $this->getDataGenerator()->create_role_capability(
            $roleid1,
            [capability_manager::CAPABILITY_APPROVE => 'allow'],
            $context
        );

        $roleid2 = $this->getDataGenerator()->create_role();
        $this->getDataGenerator()->create_role_capability(
            $roleid2,
            [capability_manager::CAPABILITY_APPROVE => 'allow'],
            $context
        );

        set_config('enableroleapproval', 1, 'local_selfcohort');
        set_config('approvalroles', "$roleid1,$roleid2", 'local_selfcohort');
        set_config('approvalrule', membership_manager::APPROVAL_RULE_ANY, 'local_selfcohort');
        set_config('adminfinalapprove', 0, 'local_selfcohort');

        $this->getDataGenerator()->role_assign($roleid1, $user->id);

        $this->assertFalse(cohort_is_member($cohort->id, $user->id));

        $this->setUser($user);

        $manager = new membership_manager();

        $result = $manager->request_membership($cohort->id, $user->id);
        $this->assertTrue($result->get_result());

        $confirm = cohort_confirm::get_record(['userid' => $user->id, 'cohortid' => $cohort->id]);
        $this->assertNotEmpty($confirm);
        $this->assertCount(2, cohort_approve::get_records(['confirmid' => $confirm->get('id')]));

        $result = $manager->approve_membership_request($cohort->id, $user->id, $roleid1);
        $this->assertTrue($result->get_result());
        $this->assertSame(get_string('confirmapproved', 'local_selfcohort'), $result->get_message());

        $this->assertTrue(cohort_is_member($cohort->id, $user->id));
        $this->assertFalse(cohort_confirm::get_record(['userid' => $user->id, 'cohortid' => $cohort->id]));
        $this->assertCount(0, cohort_approve::get_records(['confirmid' => $confirm->get('id')]));
    }

    /**
     * Test approving membership request when any role can approve + admin approval is required.
     */
    public function test_approve_membership_request_with_enableroleapproval_rule_any_role_plus_admin() {
        $this->resetAfterTest();

        $user = $this->getDataGenerator()->create_user();

        $cohort = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $context = \context_system::instance();

        $roleid1 = $this->getDataGenerator()->create_role();
        $this->getDataGenerator()->create_role_capability(
            $roleid1,
            [capability_manager::CAPABILITY_APPROVE => 'allow'],
            $context
        );

        $roleid2 = $this->getDataGenerator()->create_role();
        $this->getDataGenerator()->create_role_capability(
            $roleid2,
            [capability_manager::CAPABILITY_APPROVE => 'allow'],
            $context
        );

        set_config('enableroleapproval', 1, 'local_selfcohort');
        set_config('approvalroles', "$roleid1,$roleid2", 'local_selfcohort');
        set_config('approvalrule', membership_manager::APPROVAL_RULE_ANY, 'local_selfcohort');
        set_config('adminfinalapprove', 1, 'local_selfcohort');

        $this->getDataGenerator()->role_assign($roleid1, $user->id);

        $this->assertFalse(cohort_is_member($cohort->id, $user->id));

        $this->setUser($user);

        $manager = new membership_manager();

        $result = $manager->request_membership($cohort->id, $user->id);
        $this->assertTrue($result->get_result());

        $confirm = cohort_confirm::get_record(['userid' => $user->id, 'cohortid' => $cohort->id]);
        $this->assertNotEmpty($confirm);
        $this->assertCount(3, cohort_approve::get_records(['confirmid' => $confirm->get('id')]));

        $result = $manager->approve_membership_request($cohort->id, $user->id, $roleid1);
        $this->assertTrue($result->get_result());
        $this->assertSame(get_string('confirmapprovedbyrole', 'local_selfcohort'), $result->get_message());

        $this->assertFalse(cohort_is_member($cohort->id, $user->id));
        $this->assertCount(3, cohort_approve::get_records(['confirmid' => $confirm->get('id')]));

        $this->setAdminUser();
        $manager = new membership_manager();
        $result = $manager->approve_membership_request($cohort->id, $user->id, membership_manager::ADMIN_ROLE_ID);
        $this->assertTrue($result->get_result());
        $this->assertSame(get_string('confirmapproved', 'local_selfcohort'), $result->get_message());

        $this->assertTrue(cohort_is_member($cohort->id, $user->id));
        $this->assertFalse(cohort_confirm::get_record(['userid' => $user->id, 'cohortid' => $cohort->id]));
        $this->assertCount(0, cohort_approve::get_records(['confirmid' => $confirm->get('id')]));
    }

    /**
     * Test approving membership request when all role have to approve.
     */
    public function test_approve_membership_request_with_enableroleapproval_rule_all_roles() {
        $this->resetAfterTest();

        $userrole1 = $this->getDataGenerator()->create_user();
        $userrole2 = $this->getDataGenerator()->create_user();

        $cohort = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $context = \context_system::instance();

        $roleid1 = $this->getDataGenerator()->create_role();
        $this->getDataGenerator()->create_role_capability(
            $roleid1,
            [capability_manager::CAPABILITY_APPROVE => 'allow'],
            $context
        );

        $roleid2 = $this->getDataGenerator()->create_role();
        $this->getDataGenerator()->create_role_capability(
            $roleid2,
            [capability_manager::CAPABILITY_APPROVE => 'allow'],
            $context
        );

        set_config('enableroleapproval', 1, 'local_selfcohort');
        set_config('approvalroles', "$roleid1,$roleid2", 'local_selfcohort');
        set_config('approvalrule', membership_manager::APPROVAL_RULE_ALL, 'local_selfcohort');
        set_config('adminfinalapprove', 0, 'local_selfcohort');

        $this->getDataGenerator()->role_assign($roleid1, $userrole1->id);
        $this->getDataGenerator()->role_assign($roleid2, $userrole2->id);

        $this->assertFalse(cohort_is_member($cohort->id, $userrole1->id));

        $this->setUser($userrole1);
        $manager = new membership_manager();

        $result = $manager->request_membership($cohort->id, $userrole1->id);
        $this->assertTrue($result->get_result());

        $confirm = cohort_confirm::get_record(['userid' => $userrole1->id, 'cohortid' => $cohort->id]);
        $this->assertNotEmpty($confirm);
        $this->assertCount(2, cohort_approve::get_records(['confirmid' => $confirm->get('id')]));

        $result = $manager->approve_membership_request($cohort->id, $userrole1->id, $roleid1);
        $this->assertTrue($result->get_result());
        $this->assertSame(get_string('confirmapprovedbyrole', 'local_selfcohort'), $result->get_message());

        $this->assertFalse(cohort_is_member($cohort->id, $userrole1->id));
        $this->assertNotEmpty(cohort_confirm::get_record(['userid' => $userrole1->id, 'cohortid' => $cohort->id]));
        $this->assertCount(2, cohort_approve::get_records(['confirmid' => $confirm->get('id')]));

        $this->setUser($userrole2);
        $manager = new membership_manager();
        $result = $manager->approve_membership_request($cohort->id, $userrole1->id, $roleid2);
        $this->assertTrue($result->get_result());
        $this->assertSame(get_string('confirmapproved', 'local_selfcohort'), $result->get_message());

        $this->assertTrue(cohort_is_member($cohort->id, $userrole1->id));
        $this->assertEmpty(cohort_confirm::get_record(['userid' => $userrole1->id, 'cohortid' => $cohort->id]));
        $this->assertCount(0, cohort_approve::get_records(['confirmid' => $confirm->get('id')]));
    }

    /**
     * Test approving membership request when all role have to approve + admin.
     */
    public function test_approve_membership_request_with_enableroleapproval_rule_all_roles_plus_admin() {
        $this->resetAfterTest();

        $userrole1 = $this->getDataGenerator()->create_user();
        $userrole2 = $this->getDataGenerator()->create_user();

        $cohort = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $context = \context_system::instance();

        $roleid1 = $this->getDataGenerator()->create_role();
        $this->getDataGenerator()->create_role_capability(
            $roleid1,
            [capability_manager::CAPABILITY_APPROVE => 'allow'],
            $context
        );

        $roleid2 = $this->getDataGenerator()->create_role();
        $this->getDataGenerator()->create_role_capability(
            $roleid2,
            [capability_manager::CAPABILITY_APPROVE => 'allow'],
            $context
        );

        set_config('enableroleapproval', 1, 'local_selfcohort');
        set_config('approvalroles', "$roleid1,$roleid2", 'local_selfcohort');
        set_config('approvalrule', membership_manager::APPROVAL_RULE_ALL, 'local_selfcohort');
        set_config('adminfinalapprove', 1, 'local_selfcohort');

        $this->getDataGenerator()->role_assign($roleid1, $userrole1->id);
        $this->getDataGenerator()->role_assign($roleid2, $userrole2->id);

        $this->assertFalse(cohort_is_member($cohort->id, $userrole1->id));

        $this->setUser($userrole1);
        $manager = new membership_manager();

        $result = $manager->request_membership($cohort->id, $userrole1->id);
        $this->assertTrue($result->get_result());

        $confirm = cohort_confirm::get_record(['userid' => $userrole1->id, 'cohortid' => $cohort->id]);
        $this->assertNotEmpty($confirm);
        $this->assertCount(3, cohort_approve::get_records(['confirmid' => $confirm->get('id')]));

        $result = $manager->approve_membership_request($cohort->id, $userrole1->id, $roleid1);
        $this->assertTrue($result->get_result());
        $this->assertSame(get_string('confirmapprovedbyrole', 'local_selfcohort'), $result->get_message());

        $this->assertFalse(cohort_is_member($cohort->id, $userrole1->id));
        $this->assertNotEmpty(cohort_confirm::get_record(['userid' => $userrole1->id, 'cohortid' => $cohort->id]));
        $this->assertCount(3, cohort_approve::get_records(['confirmid' => $confirm->get('id')]));

        $this->setUser($userrole2);
        $manager = new membership_manager();
        $result = $manager->approve_membership_request($cohort->id, $userrole1->id, $roleid2);
        $this->assertTrue($result->get_result());
        $this->assertSame(get_string('confirmapprovedbyrole', 'local_selfcohort'), $result->get_message());

        $this->assertFalse(cohort_is_member($cohort->id, $userrole1->id));
        $this->assertNotEmpty(cohort_confirm::get_record(['userid' => $userrole1->id, 'cohortid' => $cohort->id]));
        $this->assertCount(3, cohort_approve::get_records(['confirmid' => $confirm->get('id')]));

        $this->setAdminUser();
        $manager = new membership_manager();
        $result = $manager->approve_membership_request($cohort->id, $userrole1->id, membership_manager::ADMIN_ROLE_ID);

        $this->assertTrue($result->get_result());
        $this->assertSame(get_string('confirmapproved', 'local_selfcohort'), $result->get_message());

        $this->assertTrue(cohort_is_member($cohort->id, $userrole1->id));
        $this->assertEmpty(cohort_confirm::get_record(['userid' => $userrole1->id, 'cohortid' => $cohort->id]));
        $this->assertCount(0, cohort_approve::get_records(['confirmid' => $confirm->get('id')]));
    }
}
