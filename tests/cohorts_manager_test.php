<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

namespace local_selfcohort;

use advanced_testcase;
use core\output\notification;

/**
 * Tests for cohort manager class.
 *
 * @package    local_selfcohort
 * @author     Dmitrii Metelkin <dmitriim@catalyst-au.net>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 * @covers \local_selfcohort\cohorts_manager
 * @covers \local_selfcohort\action_result;
 */
class cohorts_manager_test extends advanced_testcase {

    /**
     * Test can manage cohort.
     */
    public function test_manage_cohort() {
        global $DB;

        $this->resetAfterTest();

        $cohort = $this->getDataGenerator()->create_cohort();
        $manager = new cohorts_manager();

        $this->assertEmpty($DB->get_field('cohort', 'component', ['id' => $cohort->id]));
        $result = $manager->manage_cohort($cohort->id);
        $this->assertSame('local_selfcohort', $DB->get_field('cohort', 'component', ['id' => $cohort->id]));

        $cohort->component = 'local_selfcohort';
        $this->assertEquals($cohort, $result);
    }

    /**
     * Test can unmanage cohort.
     */
    public function test_unmanage_cohort() {
        global $DB;

        $this->resetAfterTest();

        $cohort = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $user1 = $this->getDataGenerator()->create_user();
        $user2 = $this->getDataGenerator()->create_user();

        $confirm = new cohort_confirm();
        $confirm->set('userid', $user1->id);
        $confirm->set('cohortid', $cohort->id);
        $confirm->save();

        $confirm = new cohort_confirm();
        $confirm->set('userid', $user2->id);
        $confirm->set('cohortid', $cohort->id);
        $confirm->save();

        $this->assertCount(1, cohort_confirm::get_records(['userid' => $user1->id, 'cohortid' => $cohort->id]));
        $this->assertCount(1, cohort_confirm::get_records(['userid' => $user2->id, 'cohortid' => $cohort->id]));

        $manager = new cohorts_manager();

        $this->assertSame('local_selfcohort', $DB->get_field('cohort', 'component', ['id' => $cohort->id]));
        $result = $manager->unmanage_cohort($cohort->id);
        $this->assertEmpty($DB->get_field('cohort', 'component', ['id' => $cohort->id]));

        $cohort->component = '';
        $this->assertEquals($cohort, $result);

        $this->assertCount(0, cohort_confirm::get_records(['userid' => $user1->id, 'cohortid' => $cohort->id]));
        $this->assertCount(0, cohort_confirm::get_records(['userid' => $user2->id, 'cohortid' => $cohort->id]));
    }

    /**
     * Test can check member of managed cohort.
     */
    public function test_is_member() {
        $this->resetAfterTest();

        $cohort1 = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $cohort2 = $this->getDataGenerator()->create_cohort(['component' => '']);
        $cohort3 = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);

        $user = $this->getDataGenerator()->create_user();

        cohort_add_member($cohort1->id, $user->id);
        cohort_add_member($cohort2->id, $user->id);

        $manager = new cohorts_manager();

        $this->assertTrue($manager->is_member($cohort1->id, $user->id));
        $this->assertFalse($manager->is_member($cohort2->id, $user->id));
        $this->assertFalse($manager->is_member($cohort3->id, $user->id));
    }

    /**
     * Test getting max members for cohort.
     */
    public function test_get_max_members() {
        $this->resetAfterTest();

        $cohort1 = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $cohort2 = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $cohort3 = $this->getDataGenerator()->create_cohort(['component' => '']);

        $settings = new cohort_settings();
        $settings->set('cohortid', $cohort1->id);
        $settings->set('maxmembers', 10);
        $settings->save();

        $manager = new cohorts_manager();

        $this->assertSame(10, $manager->get_max_members($cohort1->id));
        $this->assertSame(0, $manager->get_max_members($cohort2->id));
        $this->assertSame(0, $manager->get_max_members($cohort3->id));
    }

    /**
     * Test get_total_members.
     */
    public function test_get_total_members() {
        $this->resetAfterTest();

        $cohort1 = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $cohort2 = $this->getDataGenerator()->create_cohort(['component' => '']);
        $cohort3 = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $cohort4 = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);

        $user1 = $this->getDataGenerator()->create_user();
        $user2 = $this->getDataGenerator()->create_user();
        $user3 = $this->getDataGenerator()->create_user();

        cohort_add_member($cohort1->id, $user1->id);
        cohort_add_member($cohort2->id, $user1->id);
        cohort_add_member($cohort3->id, $user1->id);

        cohort_add_member($cohort1->id, $user2->id);
        cohort_add_member($cohort2->id, $user2->id);

        cohort_add_member($cohort1->id, $user3->id);

        $manager = new cohorts_manager();

        $this->assertSame(3, $manager->get_total_members($cohort1->id));
        $this->assertSame(2, $manager->get_total_members($cohort2->id));
        $this->assertSame(1, $manager->get_total_members($cohort3->id));
        $this->assertSame(0, $manager->get_total_members($cohort4->id));
    }

    /**
     * Test checking if can add to a cohort.
     */
    public function test_can_add_to_cohort() {
        $this->resetAfterTest();

        $cohort1 = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $cohort2 = $this->getDataGenerator()->create_cohort(['component' => '']);
        $cohort3 = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);

        $user = $this->getDataGenerator()->create_user();

        $manager = new cohorts_manager();

        $this->assertTrue($manager->can_add_to_cohort($cohort1->id, $user->id));
        $this->assertFalse($manager->can_add_to_cohort($cohort2->id, $user->id));
        $this->assertTrue($manager->can_add_to_cohort($cohort3->id, $user->id));

        cohort_add_member($cohort1->id, $user->id);
        set_config('selectmany', 1, 'local_selfcohort');

        $manager = new cohorts_manager();
        $this->assertTrue($manager->can_add_to_cohort($cohort1->id, $user->id));
        $this->assertFalse($manager->can_add_to_cohort($cohort2->id, $user->id));
        $this->assertTrue($manager->can_add_to_cohort($cohort3->id, $user->id));

        set_config('selectmany', 0, 'local_selfcohort');

        $manager = new cohorts_manager();
        $this->assertFalse($manager->can_add_to_cohort($cohort1->id, $user->id));
        $this->assertFalse($manager->can_add_to_cohort($cohort2->id, $user->id));
        $this->assertFalse($manager->can_add_to_cohort($cohort3->id, $user->id));
    }

    /**
     * Test that we can check if a cohort is full.
     */
    public function test_is_cohort_full() {
        $this->resetAfterTest();

        $user = $this->getDataGenerator()->create_user();

        $cohort1 = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $cohort2 = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $cohort3 = $this->getDataGenerator()->create_cohort(['component' => '']);

        $settings = new cohort_settings();
        $settings->set('cohortid', $cohort1->id);
        $settings->set('maxmembers', 1);
        $settings->save();

        $settings = new cohort_settings();
        $settings->set('cohortid', $cohort2->id);
        $settings->set('maxmembers', 10);
        $settings->save();

        $manager = new cohorts_manager();
        $this->assertFalse($manager->is_cohort_full($cohort1->id));
        $this->assertFalse($manager->is_cohort_full($cohort2->id));
        $this->assertFalse($manager->is_cohort_full($cohort3->id));

        cohort_add_member($cohort1->id, $user->id);
        cohort_add_member($cohort2->id, $user->id);

        $manager = new cohorts_manager();
        $this->assertTrue($manager->is_cohort_full($cohort1->id));
        $this->assertFalse($manager->is_cohort_full($cohort2->id));
        $this->assertFalse($manager->is_cohort_full($cohort3->id));
    }

    /**
     * Test adding to cohort.
     */
    public function test_add_to_cohort() {
        global $DB;

        $this->resetAfterTest();

        $user = $this->getDataGenerator()->create_user();

        $cohort1 = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $cohort2 = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $cohort3 = $this->getDataGenerator()->create_cohort(['component' => '']);

        $this->assertFalse($DB->record_exists('cohort_members', ['userid' => $user->id, 'cohortid' => $cohort1->id]));
        $this->assertFalse($DB->record_exists('cohort_members', ['userid' => $user->id, 'cohortid' => $cohort2->id]));
        $this->assertFalse($DB->record_exists('cohort_members', ['userid' => $user->id, 'cohortid' => $cohort3->id]));

        $settings = new cohort_settings();
        $settings->set('cohortid', $cohort2->id);
        $settings->set('maxmembers', -1);
        $settings->save();

        $manager = new cohorts_manager();

        $result = $manager->add_to_cohort($cohort1->id, $user->id);
        $this->assertTrue($result->get_result());
        $this->assertSame(notification::NOTIFY_SUCCESS, $result->get_messagetype());
        $this->assertSame(get_string('addedtocohort', 'local_selfcohort'), $result->get_message());
        $this->assertTrue($DB->record_exists('cohort_members', ['userid' => $user->id, 'cohortid' => $cohort1->id]));

        $result = $manager->add_to_cohort($cohort2->id, $user->id);
        $this->assertFalse($result->get_result());
        $this->assertSame(notification::NOTIFY_ERROR, $result->get_messagetype());
        $this->assertSame(get_string('cohortfull', 'local_selfcohort'), $result->get_message());
        $this->assertFalse($DB->record_exists('cohort_members', ['userid' => $user->id, 'cohortid' => $cohort2->id]));

        $result = $manager->add_to_cohort($cohort3->id, $user->id);
        $this->assertFalse($result->get_result());
        $this->assertSame(notification::NOTIFY_ERROR, $result->get_messagetype());
        $this->assertSame(get_string('invalidcohort', 'local_selfcohort'), $result->get_message());
        $this->assertFalse($DB->record_exists('cohort_members', ['userid' => $user->id, 'cohortid' => $cohort3->id]));
    }

    /**
     * Test removing from cohort.
     */
    public function test_remove_from_cohort() {
        global $DB;

        $this->resetAfterTest();

        $user = $this->getDataGenerator()->create_user();

        $cohort1 = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $cohort2 = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);
        $cohort3 = $this->getDataGenerator()->create_cohort(['component' => '']);

        cohort_add_member($cohort1->id, $user->id);
        cohort_add_member($cohort3->id, $user->id);

        $this->assertTrue($DB->record_exists('cohort_members', ['userid' => $user->id, 'cohortid' => $cohort1->id]));
        $this->assertFalse($DB->record_exists('cohort_members', ['userid' => $user->id, 'cohortid' => $cohort2->id]));
        $this->assertTrue($DB->record_exists('cohort_members', ['userid' => $user->id, 'cohortid' => $cohort3->id]));

        $manager = new cohorts_manager();
        $result = $manager->remove_from_cohort($cohort1->id, $user->id);
        $this->assertTrue($result->get_result());
        $this->assertSame(notification::NOTIFY_SUCCESS, $result->get_messagetype());
        $this->assertSame(get_string('removedfromcohort', 'local_selfcohort'), $result->get_message());
        $this->assertFalse($DB->record_exists('cohort_members', ['userid' => $user->id, 'cohortid' => $cohort1->id]));

        $result = $manager->remove_from_cohort($cohort2->id, $user->id);
        $this->assertTrue($result->get_result());
        $this->assertSame(notification::NOTIFY_SUCCESS, $result->get_messagetype());
        $this->assertSame(get_string('removedfromcohort', 'local_selfcohort'), $result->get_message());
        $this->assertFalse($DB->record_exists('cohort_members', ['userid' => $user->id, 'cohortid' => $cohort2->id]));

        $result = $manager->remove_from_cohort($cohort3->id, $user->id);
        $this->assertFalse($result->get_result());
        $this->assertSame(notification::NOTIFY_ERROR, $result->get_messagetype());
        $this->assertSame(get_string('invalidcohort', 'local_selfcohort'), $result->get_message());
        $this->assertTrue($DB->record_exists('cohort_members', ['userid' => $user->id, 'cohortid' => $cohort3->id]));
    }

    /**
     * Test checking for require confirmation for a given cohort.
     */
    public function test_require_confirm() {
        $this->resetAfterTest();

        $cohort = $this->getDataGenerator()->create_cohort(['component' => 'local_selfcohort']);

        $settings = new cohort_settings();
        $settings->set('cohortid', $cohort->id);
        $settings->set('maxmembers', 1);
        $settings->save();

        $manager = new cohorts_manager();
        $this->assertFalse($manager->require_confirm($cohort->id));

        $settings->set('confirm', 1);
        $settings->save();

        $manager = new cohorts_manager();
        $this->assertTrue($manager->require_confirm($cohort->id));
    }
}
