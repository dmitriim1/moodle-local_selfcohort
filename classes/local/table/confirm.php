<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_selfcohort\local\table;

use local_selfcohort\cohort_settings;
use core_user\fields;
use local_selfcohort\membership_manager;
use table_sql;
use renderable;
use moodle_url;
use html_writer;
use pix_icon;
use action_link;
use confirm_action;
use context;

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir . '/tablelib.php');

/**
 * List of cohort membership requests.
 *
 * @package    local_selfcohort
 * @author     Dmitrii Metelkin <dmitriim@catalyst-au.net>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class confirm extends table_sql implements renderable {

    /**
     * Page context.
     *
     * @var \context
     */
    protected $context;

    /**
     * Membership manager
     * @var \local_selfcohort\membership_manager
     */
    protected $membershipmanager;

    /**
     * A list of approval roles that a current user is allocated to.
     * @var array
     */
    protected $userroles = [];

    /**
     * Sets up the table.
     *
     * @param string $uniqueid Unique id of form.
     * @param moodle_url $url Url where this table is displayed.
     * @param int $perpage Number of rules to display per page.
     */
    public function __construct(string $uniqueid, moodle_url $url, context $context, int $perpage = 100) {
        parent::__construct($uniqueid);

        $this->define_columns([
            'user',
            'cohort',
            'members',
            'actions',
        ]);

        $this->define_headers([
            get_string('user'),
            get_string('cohort', 'cohort'),
            get_string('memberscount', 'cohort'),
            get_string('approval', 'local_selfcohort'),
        ]);

        $this->collapsible(false);
        $this->sortable(false);
        $this->pageable(true);
        $this->define_baseurl($url);

        $this->pagesize = $perpage;
        $this->context = $context;
        $this->membershipmanager = new membership_manager();
        $this->userroles = $this->membershipmanager->get_user_approval_roles($this->context);
    }

    /**
     * Generate content for column.
     *
     * @param \stdClass $data data object
     * @return string
     */
    public function col_user(\stdClass $data): string {
        return html_writer::link(new moodle_url('/user/view.php', ['id' => $data->userid]), fullname($data));
    }

    /**
     * Generate content for column.
     *
     * @param \stdClass $data data object
     * @return string
     */
    public function col_cohort(\stdClass $data): string {
        return $data->name;
    }

    /**
     * Generate content for column.
     *
     * @param \stdClass $data data object
     * @return string
     */
    public function col_members(\stdClass $data): string {
        $maxmembers = empty($data->maxmembers) ? get_string('unlimited') : $data->maxmembers;
        return $data->members . ' / ' . $maxmembers;
    }

    /**
     * Generate content for column.
     *
     * @param \stdClass $data data object
     * @return string
     */
    public function col_actions(\stdClass $data): string {
        global $OUTPUT;

        $url = clone $this->baseurl;
        $url->param('userid', $data->userid);
        $url->param('cohortid', $data->cohortid);
        $url->param('sesskey', sesskey());

        $actions = '';

        if ($this->membershipmanager->is_role_approval_enabled()) {
            foreach ($this->membershipmanager->get_approval_roles() as $roleid => $rolename) {
                $action = html_writer::div($rolename);
                if ($this->membershipmanager->has_role_approved($data->id, $roleid)) {
                    $icon = new pix_icon('i/checked', get_string('approved', 'local_selfcohort'), 'moodle', ['class' => 'green']);
                    $action .= $OUTPUT->render($icon);
                } else {
                    if (in_array($roleid, $this->userroles) || is_siteadmin()) {
                        $approveurl = clone $url;
                        $approveurl->param('confirm', 1);
                        $approveurl->param('roleid', $roleid);
                        $icon = new pix_icon('i/checked', get_string('approve'));
                        $confirmaction = new confirm_action(get_string('confirmapprove', 'local_selfcohort'));
                        $action .= $OUTPUT->render(new action_link($approveurl, '', $confirmaction, null, $icon));

                        $declineurl = clone $url;
                        $declineurl->param('confirm', 0);
                        $declineurl->param('roleid', $roleid);

                        $icon = new pix_icon('i/ne_red_mark', get_string('decline', 'local_selfcohort'));
                        $confirmaction = new confirm_action(get_string('confirmdecline', 'local_selfcohort'));
                        $action .= $OUTPUT->render(new action_link($declineurl, '', $confirmaction, null, $icon));
                    } else {
                        $icon = new pix_icon('i/questions', get_string('notyetapproved', 'local_selfcohort'));
                        $action .= $OUTPUT->render($icon);
                    }
                }

                $actions .= html_writer::div($action);
            }

            if ($this->membershipmanager->is_admin_approval_required()) {
                $action = html_writer::div(get_string('administrator'));

                if ($this->membershipmanager->has_admin_approved($data->id)) {
                    $icon = new pix_icon('i/checked', get_string('approved', 'local_selfcohort'), 'moodle', ['class' => 'green']);
                    $action .= $OUTPUT->render($icon);
                } else {
                    if (is_siteadmin()) {
                        $approveurl = clone $url;
                        $approveurl->param('confirm', 1);
                        $approveurl->param('roleid', membership_manager::ADMIN_ROLE_ID);
                        $icon = new pix_icon('i/checked', get_string('approve'));
                        $confirmaction = new confirm_action(get_string('confirmapprove', 'local_selfcohort'));
                        $action .= $OUTPUT->render(new action_link($approveurl, '', $confirmaction, null, $icon));

                        $declineurl = clone $url;
                        $declineurl->param('confirm', 0);
                        $declineurl->param('roleid', membership_manager::ADMIN_ROLE_ID);

                        $icon = new pix_icon('i/ne_red_mark', get_string('decline', 'local_selfcohort'));
                        $confirmaction = new confirm_action(get_string('confirmdecline', 'local_selfcohort'));
                        $action .= $OUTPUT->render(new action_link($declineurl, '', $confirmaction, null, $icon));
                    } else {
                        $icon = new pix_icon('i/questions', get_string('decline', 'local_selfcohort'));
                        $action .= $OUTPUT->render($icon);
                    }
                }

                $actions .= html_writer::div($action);
            }
        } else {
            $approveurl = clone $url;
            $approveurl->param('confirm', 1);
            $icon = new pix_icon('i/checked', get_string('approve'));
            $confirmaction = new confirm_action(get_string('confirmapprove', 'local_selfcohort'));
            $actions .= $OUTPUT->render(new action_link($approveurl, '', $confirmaction, null, $icon));

            $declineurl = clone $url;
            $declineurl->param('confirm', 0);

            $icon = new pix_icon('i/ne_red_mark', get_string('decline', 'local_selfcohort'));
            $confirmaction = new confirm_action(get_string('confirmdecline', 'local_selfcohort'));
            $actions .= $OUTPUT->render(new action_link($declineurl, '', $confirmaction, null, $icon));
        }

        return $actions;
    }

    /**
     * Print nothing to display message.
     */
    public function print_nothing_to_display() {
        global $OUTPUT;

        if (empty($this->rawdata)) {
            echo $OUTPUT->notification(get_string('norequests', 'local_selfcohort'), 'warning', false);
        }
    }

    /**
     * Query the reader. Store results in the object for use by build_table.
     *
     * @param int $pagesize size of page for paginated displayed table.
     * @param bool $useinitialsbar do you want to use the initials bar.
     */
    public function query_db($pagesize, $useinitialsbar = true) {
        global $DB;

        $contextsql = '';
        $params = [];
        if ($this->context->contextlevel != CONTEXT_SYSTEM) {
            $contextsql = 'AND  contextid = ? ';
            $params = [$this->context->id];
        }

        $userfields = implode(',', fields::get_name_fields());
        $sql = "SELECT conf.id, conf.userid, conf.cohortid,
                       $userfields, c.name
                  FROM {local_selfcohort_confirm} conf
                  JOIN {user} u ON u.id = conf.userid
                  JOIN {cohort} c ON (c.id = conf.cohortid $contextsql)";

        $requests = $DB->get_records_sql($sql, $params);
        $total = count($requests);

        if (!empty($requests)) {
            $members = $DB->get_records_sql('SELECT cohortid, COUNT(*) FROM {cohort_members} GROUP BY cohortid');
            $settings = [];
            foreach (cohort_settings::get_records() as $setting) {
                $settings[$setting->get('cohortid')] = $setting;
            }
            $this->pagesize($pagesize, $total);
            $this->rawdata = array_slice($requests, ($pagesize * $this->currpage), $pagesize);

            foreach ($this->rawdata as $data) {
                $data->members = $members[$data->cohortid]->count ?? 0;
                $data->maxmembers = isset($settings[$data->cohortid]) ? $settings[$data->cohortid]->get('maxmembers') : 0;
            }
        }

        if ($useinitialsbar) {
            $this->initialbars($total > $pagesize);
        }
    }
}
