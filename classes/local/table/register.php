<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_selfcohort\local\table;

use local_selfcohort\membership_manager;
use local_selfcohort\cohorts_manager;
use table_sql;
use renderable;
use moodle_url;
use html_writer;

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir . '/tablelib.php');

/**
 * List of cohort to be registered for.
 *
 * @package    local_selfcohort
 * @author     Dmitrii Metelkin <dmitriim@catalyst-au.net>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class register extends table_sql implements renderable {

    /**
     * Cohort manager.
     * @var cohorts_manager
     */
    protected $cohortmanager;

    /**
     * Membership manager
     * @var \local_selfcohort\membership_manager
     */
    protected $membershipmanager;

    /**
     * Current user id,
     * @var int
     */
    protected $userid;

    /**
     * Sets up the table.
     *
     * @param string $uniqueid Unique id of form.
     * @param moodle_url $url Url where this table is displayed.
     * @param int $perpage Number of rules to display per page.
     */
    public function __construct(string $uniqueid, moodle_url $url, int $perpage = 100) {
        global $USER;

        parent::__construct($uniqueid);

        $this->define_columns([
            'name',
            'members',
            'membership',
            'actions',
        ]);

        $this->define_headers([
            get_string('cohort', 'cohort'),
            get_string('memberscount', 'cohort'),
            get_string('memberq', 'local_selfcohort'),
            get_string('actions'),
        ]);

        $this->collapsible(false);
        $this->sortable(false);
        $this->pageable(true);
        $this->define_baseurl($url);

        $this->pagesize = $perpage;
        $this->userid = $USER->id;
        $this->cohortmanager = new cohorts_manager();
        $this->membershipmanager = new membership_manager();
    }

    /**
     * Generate content for column.
     *
     * @param \stdClass $data data object
     * @return string
     */
    public function col_name(\stdClass $data): string {
        return $data->name;
    }

    /**
     * Generate content for column.
     *
     * @param \stdClass $data data object
     * @return string
     */
    public function col_membership(\stdClass $data): string {
        global $OUTPUT;

        if ($this->cohortmanager->is_member($data->id, $this->userid)) {
            return $OUTPUT->pix_icon('i/valid', '', 'moodle', ['title' => '']);
        } else {
            return $OUTPUT->pix_icon('i/ne_red_mark', '', 'moodle', ['title' => '']);
        }
    }

    /**
     * Generate content for column.
     *
     * @param \stdClass $data data object
     * @return string
     */
    public function col_actions(\stdClass $data): string {
        global $OUTPUT;

        $ismember = $this->cohortmanager->is_member($data->id, $this->userid);

        $url = clone $this->baseurl;
        $text = !empty($ismember) ? get_string('removeme', 'local_selfcohort') : get_string('addme', 'local_selfcohort');
        $icon = !empty($ismember) ? 'i/delete' : 'i/assignroles';
        !empty($ismember) ? $url->param('action', 'remove') : $url->param('action', 'add');

        $url->param('cohortid', $data->id);
        $url->param('sesskey', sesskey());

        // Already a member of cohort.
        if ($this->cohortmanager->is_member($data->id, $this->userid)) {
            return html_writer::link($url, $OUTPUT->pix_icon($icon, $text, 'moodle', ['title' => $text]) . $text);
        }

        // Cohort is full.
        if ($this->cohortmanager->is_cohort_full($data->id)) {
            $text = get_string('cohortfull', 'local_selfcohort');
            $icon = 'i/caution';
            return $OUTPUT->pix_icon($icon, $text, 'moodle', ['title' => $text]) . $text;
        }

        // Requested membership.
        if ($this->membershipmanager->is_membership_requested($data->id, $this->userid)) {
            $text = get_string('confirmpending', 'local_selfcohort');
            $icon = 'i/info';
            return $OUTPUT->pix_icon($icon, $text, 'moodle', ['title' => $text]) . $text;
        }

        // Can't add to cohort.
        if (!$this->cohortmanager->can_add_to_cohort($data->id, $this->userid)) {
            $text = get_string('canselectonecohort', 'local_selfcohort');
            $icon = 'i/info';
            return $OUTPUT->pix_icon($icon, $text, 'moodle', ['title' => $text]) . $text;
        }

        // Requires confirmation.
        if ($this->cohortmanager->require_confirm($data->id)) {
            $text = get_string('requestconfirm', 'local_selfcohort');
            $url->param('action', 'request');
            return html_writer::link($url, $OUTPUT->pix_icon($icon, $text, 'moodle', ['title' => $text])  . $text);
        }

        // Add to cohort.
        return html_writer::link($url, $OUTPUT->pix_icon($icon, $text, 'moodle', ['title' => $text])  . $text);
    }

    /**
     * Generate content for column.
     *
     * @param \stdClass $data data object
     * @return string
     */
    public function col_members(\stdClass $data): string {
        if ($this->cohortmanager->get_max_members($data->id) == 0) {
            $members = get_string('unlimited');
        } else {
            $members = $this->cohortmanager->get_total_members($data->id) . '/' . $this->cohortmanager->get_max_members($data->id);
        }

        return $members;
    }

    /**
     * This function is not part of the public api.
     */
    public function print_nothing_to_display() {
        global $OUTPUT;

        if (empty($this->rawdata)) {
            echo $OUTPUT->notification(get_string('nocohorts', 'local_selfcohort'), 'warning', false);
        }
    }

    /**
     * Query the reader. Store results in the object for use by build_table.
     *
     * @param int $pagesize size of page for paginated displayed table.
     * @param bool $useinitialsbar do you want to use the initials bar.
     */
    public function query_db($pagesize, $useinitialsbar = true) {
        global $DB;

        $cohorts = $DB->get_records('cohort', ['component' => 'local_selfcohort'], 'name');
        $total = count($cohorts);

        if (!empty($cohorts)) {
            $this->pagesize($pagesize, $total);
            $this->rawdata = array_slice($cohorts, ($pagesize * $this->currpage), $pagesize);
        }

        if ($useinitialsbar) {
            $this->initialbars($total > $pagesize);
        }
    }
}
