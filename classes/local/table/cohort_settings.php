<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_selfcohort\local\table;

use local_selfcohort\output\inplace_editable_confirm;
use local_selfcohort\output\inplace_editable_maxmembers;
use local_selfcohort\output\inplace_editable_selfreg;
use local_selfcohort\cohort_settings as settings;
use table_sql;
use renderable;
use moodle_url;
use context;

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir . '/tablelib.php');

/**
 * List of cohort settings records.
 *
 * @package    local_selfcohort
 * @author     Dmitrii Metelkin <dmitriim@catalyst-au.net>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class cohort_settings extends table_sql implements renderable {

    /**
     * A cached list of cohort settings.
     *
     * @var settings[]
     */
    protected $cohortsettings = [];

    /**
     * @var \context
     */
    protected $context;

    /**
     * Sets up the table.
     *
     * @param string $uniqueid Unique id of form.
     * @param moodle_url $url Url where this table is displayed.
     * @param int $perpage Number of rules to display per page.
     */
    public function __construct(string $uniqueid, moodle_url $url, context $context, int $perpage = 100) {
        parent::__construct($uniqueid);

        $this->define_columns([
            'name',
            'selfreg',
            'confirm',
            'maxmembers',
            'members',
            'actions',
        ]);

        $this->define_headers([
            get_string('cohort', 'cohort'),
            get_string('selfreg', 'local_selfcohort'),
            get_string('requiresconfirm', 'local_selfcohort'),
            get_string('maxmembers', 'local_selfcohort'),
            get_string('memberscount', 'cohort'),
            get_string('actions'),
        ]);

        $this->collapsible(false);
        $this->sortable(false);
        $this->pageable(true);
        $this->define_baseurl($url);

        $this->pagesize = $perpage;
        $this->context = $context;
    }

    /**
     * Generate content for column.
     *
     * @param \stdClass $data data object
     * @return string
     */
    public function col_name(\stdClass $data): string {
        return $data->name;
    }

    /**
     * Generate content for column.
     *
     * @param \stdClass $data data object
     * @return string
     */
    public function col_selfreg(\stdClass $data): string {
        global $OUTPUT;

        $selfreg = new inplace_editable_selfreg($data);
        return $selfreg->render($OUTPUT);

    }

    /**
     * Generate content for column.
     *
     * @param \stdClass $data data object
     * @return string
     */
    public function col_confirm(\stdClass $data): string {
        global $OUTPUT;

        $settings = $this->cohortsettings[$data->id];
        $confirm = new inplace_editable_confirm($settings);
        return $confirm->render($OUTPUT);
    }

    /**
     * Generate content for column.
     *
     * @param \stdClass $data data object
     * @return string
     */
    public function col_members(\stdClass $data): string {
        return (string) $data->members;
    }

    /**
     * Generate content for column.
     *
     * @param \stdClass $data data object
     * @return string
     */
    public function col_maxmembers(\stdClass $data): string {
        global $OUTPUT;

        $settings = $this->cohortsettings[$data->id];
        $maxmembers = new inplace_editable_maxmembers($settings);
        return $maxmembers->render($OUTPUT);
    }

    /**
     * Generate content for column.
     *
     * @param \stdClass $data data object
     * @return string
     */
    public function col_actions(\stdClass $data): string {
        global $OUTPUT;

        $actions = '';

        if (has_capability('moodle/cohort:manage', \context::instance_by_id($data->contextid))) {

            $returnurl = clone $this->baseurl;
            $returnurl->param('contextid', $this->context->id);

            if ($this->context instanceof \context_system) {
                $returnurl->param('showall', 1);
            }

            $visibilityurl = new moodle_url('/local/selfcohort/edit.php', [
                'id' => $data->id,
                'contextid' => $data->contextid,
                'sesskey' => sesskey(),
                'returnurl' => $returnurl,

            ]);

            if ($data->visible) {
                $icon = $OUTPUT->render(new \pix_icon('t/hide', get_string('hide')));
                $visibilityurl->param('hide', 1);
            } else {
                $icon = $OUTPUT->render(new \pix_icon('t/show', get_string('show')));
                $visibilityurl->param('show', 1);
            }

            $actions .= \html_writer::link($visibilityurl, $icon, ['class' => 'action-icon']);

            $editurl = new moodle_url('/local/selfcohort/edit.php', [
                'id' => $data->id,
                'contextid' => $data->contextid,
                'sesskey' => sesskey(),
                'edit' => 1,
                'returnurl' => $returnurl,
            ]);
            $icon = $OUTPUT->render(new \pix_icon('t/edit', get_string('edit')));
            $actions .= \html_writer::link($editurl, $icon, ['class' => 'action-icon']);

            $deleteurl = new moodle_url('/local/selfcohort/edit.php', [
                'id' => $data->id,
                'contextid' => $data->contextid,
                'sesskey' => sesskey(),
                'delete' => 1,
                'returnurl' => $returnurl,
            ]);
            $icon = $OUTPUT->render(new \pix_icon('t/delete', get_string('delete')));
            $actions .= \html_writer::link($deleteurl, $icon, ['class' => 'action-icon']);
        }

        return $actions;
    }

    /**
     * Get any extra classes names to add to this row in the HTML.
     *
     * @param \stdClass $data the data for this row.
     *
     * @return string added to the class="" attribute of the tr.
     */
    public function get_row_class($data) {
        if (!$data->visible) {
            return 'text-muted';
        }

        return '';
    }

    /**
     * Query the reader. Store results in the object for use by build_table.
     *
     * @param int $pagesize size of page for paginated displayed table.
     * @param bool $useinitialsbar do you want to use the initials bar.
     */
    public function query_db($pagesize, $useinitialsbar = true) {
        global $DB;

        $contextsql = '';
        $params = [];
        if ($this->context->contextlevel != CONTEXT_SYSTEM) {
            $contextsql = 'AND  contextid = ? ';
            $params = [$this->context->id];
        }

        $select = "(component = '' OR component = 'local_selfcohort') $contextsql";
        $cohorts = $DB->get_records_select(
            'cohort',
            $select,
            $params,
            'name', 'id, name, component, visible, contextid, component'
        );

        foreach (settings::get_records() as $setting) {
            $this->cohortsettings[$setting->get('cohortid')] = $setting;
        }

        $total = count($cohorts);
        $this->pagesize($pagesize, $total);
        $this->rawdata = array_slice($cohorts, ($pagesize * $this->currpage), $pagesize);

        foreach ($this->rawdata as $data) {
            // Create settings if not exist.
            if (!isset($this->cohortsettings[$data->id])) {
                $settings = new settings(0, (object)['cohortid' => $data->id]);
                $settings->create();
                $this->cohortsettings[$data->id] = $settings;
            }

            $data->members = $DB->count_records('cohort_members', ['cohortid' => $data->id]);
            $data->selfreg = ($data->component == 'local_selfcohort') ? 1 : 0;
            $data->maxmembers = $this->cohortsettings[$data->id]->get('maxmembers');
            $data->confirm = $this->cohortsettings[$data->id]->get('confirm');
        }

        if ($useinitialsbar) {
            $this->initialbars($total > $pagesize);
        }
    }
}
