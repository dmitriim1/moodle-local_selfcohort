<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

namespace local_selfcohort\event;

use core\event\base;
use context_system;

/**
 * Cohort membership approved event class.
 *
 * @package    local_selfcohort
 * @author     Dmitrii Metelkin <dmitriim@catalyst-au.net>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class membership_request_approved extends base {

    /**
     * Initialise the data.
     */
    protected function init() {
        $this->data['edulevel'] = self::LEVEL_OTHER;
        $this->data['crud'] = 'c';
        $this->context = context_system::instance();
    }

    /**
     * Return localised event name.
     *
     * @return string
     */
    public static function get_name(): string {
        return get_string('event:membership_request_approved', 'local_selfcohort');
    }

    /**
     * Returns description of what happened.
     *
     * @return string
     */
    public function get_description(): string {
        return "User with id '{$this->userid}' approved membership request of user with id '{$this->other['requester']}'"
         . " to cohort with id '{$this->other['cohortid']}'";
    }

    /**
     * Validates the custom data.
     */
    protected function validate_data() {
        parent::validate_data();

        if (!isset($this->other['requester'])) {
            throw new \coding_exception('The \'requester\' value must be set in other.');
        }

        if (!isset($this->other['cohortid'])) {
            throw new \coding_exception('The \'cohortid\' value must be set in other.');
        }
    }
}
