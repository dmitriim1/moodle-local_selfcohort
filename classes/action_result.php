<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_selfcohort;

use core\output\notification;

/**
 * Class represent a result of membership manager actions.
 *
 * @package    local_selfcohort
 * @author     Dmitrii Metelkin <dmitriim@catalyst-au.net>
 * @copyright  Catalyst IT
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class action_result {

    /**
     * Result message.
     * @var string
     */
    private $message;

    /**
     * Result message type.
     * @var string
     */
    private $messagetype;

    /**
     * Actual result.
     * @var bool
     */
    private $result;

    /**
     * Constructor.
     *
     * @param bool $result Result.
     * @param string $message Message.
     */
    public function __construct(bool $result, string $message) {
        $this->result = $result;
        $this->message = $message;
        $this->messagetype = $result ? notification::NOTIFY_SUCCESS : notification::NOTIFY_ERROR;
    }

    /**
     * @return string
     */
    public function get_message(): string {
        return $this->message;
    }

    /**
     * @return string
     */
    public function get_messagetype(): string {
        return $this->messagetype;
    }

    /**
     * @return bool
     */
    public function get_result(): bool {
        return $this->result;
    }
}
