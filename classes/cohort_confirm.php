<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

namespace local_selfcohort;

use core\persistent;

/**
 * Cohort confirmation records.
 *
 * @package    local_selfcohort
 * @author     Dmitrii Metelkin <dmitriim@catalyst-au.net>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class cohort_confirm extends persistent {

    /** @var string DB table */
    public const TABLE = 'local_selfcohort_confirm';

    /**
     * Defines properties.
     * @return array
     */
    protected static function define_properties(): array {
        return [
            'cohortid' => [
                'type' => PARAM_INT,
            ],
            'userid' => [
                'type' => PARAM_INT,
            ],
        ];
    }

    /**
     * Delete all records.
     *
     * @return void
     */
    public static function purge_all(): void {
        foreach (self::get_records() as $record) {
            $record->delete();
        }
    }
}
