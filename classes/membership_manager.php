<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

namespace local_selfcohort;

use context;
use core\message\message;
use core_user;
use local_selfcohort\event\membership_request_approved;
use local_selfcohort\event\membership_request_approved_by_role;
use local_selfcohort\event\membership_request_declined;
use local_selfcohort\event\membership_requested;
use moodle_url;
use required_capability_exception;
use coding_exception;

/**
 * Class responsible for managing confirmation requests.
 *
 * @package    local_selfcohort
 * @author     Dmitrii Metelkin <dmitriim@catalyst-au.net>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class membership_manager {

    /**
     * Requires all roles to approve before a confirmation request is counted as approved.
     */
    public const APPROVAL_RULE_ALL = 0;

    /**
     * Requires any role to approve before a confirmation request is counted as approved.
     */
    public const APPROVAL_RULE_ANY = 1;

    /**
     * Admin role ID.
     */
    public const ADMIN_ROLE_ID = 0;

    /**
     * Is role approval feature enabled?
     * @var bool
     */
    private $enableroleapproval;

    /**
     * A lis of role IDs that can approve.
     *
     * @var array
     */
    private $approvalroles;

    /**
     * Is admin approval required.
     * @var bool
     */
    private $adminfinalapprove;

    /**
     * Approval rule.
     * @see self::APPROVAL_RULE_ALL
     * @see self::APPROVAL_RULE_ANY
     *
     * @var int
     */
    private $approvalrule;

    /**
     * Should notify site administrators about new membership requests.
     *
     * @var bool
     */
    private $notifyadmins;

    /**
     * Cohort manager instance.
     *
     * @var \local_selfcohort\cohorts_manager
     */
    private $cohortmanager;

    /**
     * Class constructor.
     */
    public function __construct() {
        $config = get_config('local_selfcohort');

        $this->enableroleapproval = $config->enableroleapproval ?? false;
        $this->approvalrule = $config->approvalrule ?? self::APPROVAL_RULE_ANY;
        $this->adminfinalapprove = $config->adminfinalapprove ?? false;
        $this->notifyadmins = $config->notifyadmins ?? false;

        $this->approvalroles = [];
        if (!empty($config->approvalroles) && is_string($config->approvalroles)) {
            $approvalroles = explode(',', $config->approvalroles);
        }

        // Make sure all configured roles have required capability.
        if (!empty($approvalroles)) {
            $capableroles = get_roles_with_capability(capability_manager::CAPABILITY_APPROVE, CAP_ALLOW);
            foreach ($approvalroles as $key => $roleid) {
                if (key_exists($roleid, $capableroles)) {
                    $this->approvalroles[$roleid] = role_get_name($capableroles[$roleid]);
                }
            }
        }

        $this->cohortmanager = new cohorts_manager();
    }

    /**
     * Check if membership has been requested for a given user to a given cohort.
     *
     * @param int $cohortid Cohort ID.
     * @param int $userid User ID.
     *
     * @return bool
     */
    public function is_membership_requested(int $cohortid, int $userid): bool {
        return cohort_confirm::count_records(['cohortid' => $cohortid, 'userid' => $userid]) > 0;
    }

    /**
     * Generate confirmation request to add a given user to a cohort.
     *
     * @param int $cohortid Cohort ID,
     * @param int $userid User ID.
     *
     * @return action_result
     */
    public function request_membership(int $cohortid, int $userid): action_result {
        if (!$this->cohortmanager->is_managed_cohort($cohortid)) {
            return new action_result(false, get_string('invalidcohort', 'local_selfcohort'));
        }

        if ($this->cohortmanager->is_cohort_full($cohortid)) {
            return new action_result(false, get_string('cohortfull', 'local_selfcohort'));
        }

        $confirm = cohort_confirm::get_record(['cohortid' => $cohortid, 'userid' => $userid]);

        if (!$confirm) {
            $confirm = new cohort_confirm(0, (object) ['cohortid' => $cohortid, 'userid' => $userid]);
            $confirm->save();

            if ($this->is_role_approval_enabled()) {
                $this->request_approval($confirm);
            }

            $this->send_membership_request_notifications($cohortid, $userid);

            membership_requested::create([
                'userid' => $userid,
                'other' => [
                    'cohortid' => $cohortid,
                ],
            ])->trigger();
        }

        return new action_result(true, get_string('confirmrequested', 'local_selfcohort'));
    }


    /**
     * Check if role approval feature is enabled.
     *
     * @return bool
     */
    public function is_role_approval_enabled(): bool {
        if (!empty($this->enableroleapproval) && !empty($this->approvalroles)) {
            return true;
        }

        return false;
    }

    /**
     * Get list of all approval roles.
     *
     * @return array
     */
    public function get_approval_roles(): array {
        return $this->approvalroles;
    }

    /**
     * Check if admin approval required.
     *
     * @return bool
     */
    public function is_admin_approval_required(): bool {
        return !empty($this->adminfinalapprove) && $this->is_role_approval_enabled();
    }

    /**
     * Get a list of roles with approval capability for a current user in a given context.
     *
     * @param context $context Context
     *
     * @return array
     */
    public function get_user_approval_roles(context $context): array {
        $approvingoles = [];

        $roles = get_user_roles($context);
        foreach ($roles as $role) {
            if (array_key_exists($role->roleid, $this->approvalroles)) {
                $approvingoles[] = $role->roleid;
            }
        }

        return $approvingoles;
    }

    /**
     * Decline confirmation request to add a given user to a cohort.
     *
     * @param int $cohortid Cohort ID,
     * @param int $userid User ID.
     * @param ?int $roleid User role ID.
     *
     * @return action_result
     */
    public function decline_membership_request(int $cohortid, int $userid, ?int $roleid = null): action_result {
        if (!$this->cohortmanager->is_managed_cohort($cohortid)) {
            return new action_result(false, get_string('invalidcohort', 'local_selfcohort'));
        }

        $confirm = cohort_confirm::get_record(['cohortid' => $cohortid, 'userid' => $userid]);

        if (!$confirm) {
            return new action_result(false, get_string('invalidconfirm', 'local_selfcohort'));
        }

        if ($this->is_role_approval_enabled()) {
            if (is_null($roleid)) {
                throw new coding_exception('Role ID is required during declining when roles approval feature is enabled.');
            }

            $this->validate_approve_decline_permissions($cohortid, $roleid);
            cohort_approve::purge_for_confirmation($confirm->get('id'));
        }

        $confirm->delete();
        $this->send_request_result_notification($userid, $cohortid, 'decline');

        membership_request_declined::create([
            'other' => [
                'cohortid' => $cohortid,
                'requester' => $userid,
            ],
        ])->trigger();

        return new action_result(true, get_string('confirmdeclined', 'local_selfcohort'));
    }

    /**
     * Approve confirmation request to add a given user to a cohort.
     *
     * @param int $cohortid Cohort ID,
     * @param int $userid User ID.
     * @param ?int $roleid User role ID.
     *
     * @return action_result
     */
    public function approve_membership_request(int $cohortid, int $userid, ?int $roleid = null): action_result {
        $confirm = cohort_confirm::get_record(['cohortid' => $cohortid, 'userid' => $userid]);

        if (!$confirm) {
            return new action_result(false, get_string('invalidconfirm', 'local_selfcohort'));
        }

        if ($this->is_role_approval_enabled()) {
            if (is_null($roleid)) {
                throw new coding_exception('Role ID is required during approval when roles approval feature is enabled.');
            }

            $this->validate_approve_decline_permissions($cohortid, $roleid);

            $approval = cohort_approve::get_record(['confirmid' => $confirm->get('id'), 'roleid' => $roleid]);
            if (!$approval) {
                return new action_result(false, get_string('invalidapproval', 'local_selfcohort'));
            }

            $approval->set('status', cohort_approve::STATUS_APPROVED);
            $approval->save();

            membership_request_approved_by_role::create([
                'other' => [
                    'cohortid' => $cohortid,
                    'requester' => $userid,
                    'roleid' => $roleid,
                ],
            ])->trigger();

            // Not fully approved yet.
            if (!$this->is_membership_approved($confirm->get('id'))) {
                return new action_result(true, get_string('confirmapprovedbyrole', 'local_selfcohort'));
            }
        }

        $result = $this->cohortmanager->add_to_cohort($cohortid, $userid);
        if (!$result->get_result()) {
            return $result;
        }

        // Purge all approvals.
        cohort_approve::purge_for_confirmation($confirm->get('id'));
        $confirm->delete();
        $this->send_request_result_notification($userid, $cohortid, 'approve');

        membership_request_approved::create([
            'other' => [
                'cohortid' => $cohortid,
                'requester' => $userid,
            ],
        ])->trigger();

        return new action_result(true, get_string('confirmapproved', 'local_selfcohort'));
    }

    /**
     * Check if membership is approved for a given confirmation request.
     *
     * @param int $confirmid Confirmation request ID.
     *
     * @return bool
     */
    private function is_membership_approved(int $confirmid): bool {
        $adminapproved = empty($this->is_admin_approval_required()) || $this->has_admin_approved($confirmid);

        switch ($this->approvalrule) {

            case self::APPROVAL_RULE_ANY:
                $params = ['status' => cohort_approve::STATUS_APPROVED, 'confirmid' => $confirmid];
                $rolesapproved = !empty(cohort_approve::get_records($params));
                break;

            case self::APPROVAL_RULE_ALL:
                $rolesapproved = false;
                foreach ($this->approvalroles as $roleid => $rolename) {
                    if (empty($this->has_role_approved($confirmid, $roleid))) {
                        $rolesapproved = false;
                        break;
                    } else {
                        $rolesapproved = true;
                    }
                }
                break;

            default:
                $rolesapproved = false;
        }

        return $rolesapproved && $adminapproved;
    }

    /**
     * Check if admin has approved given confirmation request.
     *
     * @param int $confirmid Confirmation request ID.
     *
     * @return bool
     */
    public function has_admin_approved(int $confirmid): bool {
        return !empty(cohort_approve::get_records([
            'status' => cohort_approve::STATUS_APPROVED,
            'confirmid' => $confirmid,
            'roleid' => self::ADMIN_ROLE_ID,
        ]));
    }

    /**
     * Check if admin has approved given confirmation request.
     *
     * @param int $confirmid Confirmation request ID.
     * @param int $roleid Role ID to check.
     *
     * @return bool
     */
    public function has_role_approved(int $confirmid, int $roleid): bool {
        return !empty(cohort_approve::get_records([
            'status' => cohort_approve::STATUS_APPROVED,
            'confirmid' => $confirmid,
            'roleid' => $roleid,
        ]));
    }

    /**
     * Request approval for a given confirmation request.
     *
     * @param cohort_confirm $confirm Confirmation request.
     */
    private function request_approval(cohort_confirm $confirm): void {
        cohort_approve::purge_for_confirmation($confirm->get('id'));

        foreach ($this->approvalroles as $roleid => $rolename) {
            $approval = new cohort_approve(0, (object) [
                'confirmid' => $confirm->get('id'),
                'roleid' => $roleid,
                'status' => cohort_approve::STATUS_NEW,
            ]);
            $approval->save();
        }

        if ($this->is_admin_approval_required()) {
            $approval = new cohort_approve(0, (object) [
                'confirmid' => $confirm->get('id'),
                'roleid' => self::ADMIN_ROLE_ID,
                'status' => cohort_approve::STATUS_NEW,
            ]);
            $approval->save();
        }
    }

    /**
     * Send a result of processing a user membership request.
     *
     * @param int $userid User ID the requested a membership.
     * @param int $cohortid Cohort ID.
     * @param string $result Result string (approve, decline).
     */
    private function send_request_result_notification(int $userid, int $cohortid, string $result) {
        $user = core_user::get_user($userid);
        $cohort = $this->get_cohort($cohortid);

        $fullmessage = get_string('requestfullmessage' . $result, 'local_selfcohort', (object)[
            'userid' => $userid,
            'userfullname' => fullname($user),
            'cohortid' => $cohortid,
            'cohortname' => !empty($cohort) ? $cohort->name : '',
        ]);

        $message = new message();
        $message->component = 'local_selfcohort';
        $message->name = 'confirm';
        $message->userfrom = core_user::get_noreply_user();
        $message->userto = $user;
        $message->subject = get_string('requestsubject' . $result, 'local_selfcohort');
        $message->fullmessage = $fullmessage;
        $message->fullmessageformat = FORMAT_HTML;
        $message->fullmessagehtml = $fullmessage;
        $message->smallmessage = $fullmessage;
        $message->notification = 1;
        message_send($message);
    }

    /**
     * Gets cohort record.
     *
     * @param int $cohortid Cohort ID.
     *
     * @return ?\stdClass
     */
    private function get_cohort(int $cohortid): ?\stdClass {
        global $DB;

        $cohort = $DB->get_record('cohort', ['id' => $cohortid]);

        return !empty($cohort) ? $cohort : null;
    }

    /**
     * Get cohort context.
     *
     * @param int $cohortid Cohort ID.
     *
     * @return context
     */
    private function get_cohort_context(int $cohortid): ?context {
        $cohort = $this->get_cohort($cohortid);
        return !empty($cohort) ? context::instance_by_id($cohort->contextid) : null;
    }

    /**
     * Send membership request notifications.
     *
     * @param int $cohortid Cohort ID the request is for.
     * @param int $userid User ID the request was generated by.
     */
    private function send_membership_request_notifications(int $cohortid, int $userid) {
        $recipients = $this->get_membership_request_notification_recipients($cohortid);

        if (!empty($recipients)) {
            $cohort = $this->get_cohort($cohortid);

            $user = core_user::get_user($userid);
            $fullmessage = get_string('requestfullmessage', 'local_selfcohort', (object)[
                'userid' => $userid,
                'userfullname' => fullname($user),
                'cohortid' => $cohortid,
                'cohortname' => !empty($cohort) ? $cohort->name : '',
            ]);

            $message = new message();
            $message->component = 'local_selfcohort';
            $message->name = 'request';
            $message->userfrom = core_user::get_noreply_user();
            $message->subject = get_string('requestsubject', 'local_selfcohort');
            $message->fullmessage = $fullmessage;
            $message->fullmessageformat = FORMAT_HTML;
            $message->fullmessagehtml = $fullmessage;
            $message->smallmessage = $fullmessage;
            $message->notification = 1;
            $message->contexturl = (new moodle_url('/local/selfcohort/cohorts.php', ['action' => 'confirm']))->out(false);
            $message->contexturlname = get_string('confirmrequests', 'local_selfcohort');

            foreach ($recipients as $recipient) {
                $message->userto = $recipient;
                message_send($message);
            }
        }
    }

    /**
     * Validate permissions.
     *
     * @param int $cohortid Cohort ID
     * @param int $roleid Role ID.
     */
    private function validate_approve_decline_permissions(int $cohortid, int $roleid): void {
        if (!is_siteadmin()) {
            $context = $this->get_cohort_context($cohortid);
            if (!in_array($roleid, $this->get_user_approval_roles($context))) {
                throw new required_capability_exception($context, capability_manager::CAPABILITY_APPROVE, 'nopermissions', '');
            }
        }
    }

    /**
     * Get list of notification recipients.
     *
     * @param int $cohortid Cohort ID.
     *
     * @return array
     */
    private function get_membership_request_notification_recipients(int $cohortid): array {
        $context = $this->get_cohort_context($cohortid);
        $recipients = get_users_by_capability($context, capability_manager::CAPABILITY_RECEIVE_NOTIFICATION);

        if ($this->notifyadmins) {
            foreach (get_admins() as $admin) {
                $recipients[] = $admin;
            }
        }

        return $recipients;
    }
}
