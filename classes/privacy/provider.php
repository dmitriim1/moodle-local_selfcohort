<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Privacy Subsystem implementation for local_selfcohort
 *
 * @package     local_selfcohort
 * @copyright   2020 Daniel Neis Araujo <daniel@adapta.online>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_selfcohort\privacy;

use core_privacy\local\metadata\collection;
use core_privacy\local\request\approved_contextlist;
use core_privacy\local\request\approved_userlist;
use core_privacy\local\request\contextlist;
use core_privacy\local\request\userlist;
use core_privacy\local\request\writer;

/**
 * Privacy Subsystem for local_selfcohort implementing null_provider.
 *
 * @copyright   2020 Daniel Neis Araujo <daniel@adapta.online>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class provider implements \core_privacy\local\metadata\provider,
\core_privacy\local\request\core_userlist_provider,
\core_privacy\local\request\plugin\provider {

    /**
     * Retrieve the user metadata stored by plugin.
     *
     * @param collection $collection Collection of metadata.
     * @return collection Collection of metadata.
     */
    public static function get_metadata(collection $collection): collection {
        $collection->add_database_table(
            'local_selfcohort',
            [
                'cohortid' => 'privacy:metadata:local_selfcohort:cohortid',
                'usermodified' => 'privacy:metadata:local_selfcohort:usermodified',
            ],
            'privacy:metadata:local_selfcohort'
        );

        $collection->add_database_table(
            'local_selfcohort_confirm',
            [
                'cohortid' => 'privacy:metadata:local_selfcohort_confirm:cohortid',
                'userid' => 'privacy:metadata:local_selfcohort_confirm:userid',
                'usermodified' => 'privacy:metadata:local_selfcohort_confirm:usermodified',
            ],
            'privacy:metadata:local_selfcohort_confirm'
        );

        $collection->add_database_table(
            'local_selfcohort_approve',
            [
                'usermodified' => 'privacy:metadata:local_selfcohort_approve:usermodified',
            ],
            'privacy:metadata:local_selfcohort_approve'
        );

        return $collection;
    }

    /**
     * Get the list of contexts that contain user information for the specified user.
     *
     * @param int $userid
     *
     * @return \core_privacy\local\request\contextlist
     */
    public static function get_contexts_for_userid(int $userid): contextlist {
        $contextlist = new contextlist();

        $params = [
            'usermodified' => $userid,
        ];

        $sql = "SELECT c.contextid
                  FROM {cohort} c
            INNER JOIN {local_selfcohort} ls ON ls.cohortid = c.id
                 WHERE ls.usermodified = :usermodified";

        $contextlist->add_from_sql($sql, $params);

        $sql = "SELECT c.contextid
                  FROM {cohort} c
            INNER JOIN {local_selfcohort_confirm} ls ON ls.cohortid = c.id
            INNER JOIN {local_selfcohort_approve} lsa ON lsa.confirmid = ls.id                                                      
                 WHERE lsa.usermodified = :usermodified";

        $contextlist->add_from_sql($sql, $params);

        $params = [
            'usermodified' => $userid,
            'userid' => $userid,
        ];
        $sql = "SELECT c.contextid
                  FROM {cohort} c
            INNER JOIN {local_selfcohort_confirm} ls ON ls.cohortid = c.id
                 WHERE ls.usermodified = :usermodified OR ls.userid = :userid";

        $contextlist->add_from_sql($sql, $params);

        return $contextlist;
    }

    /**
     * Export all user data for the specified user, in the specified contexts.
     *
     * @param \core_privacy\local\request\approved_contextlist $contextlist
     */
    public static function export_user_data(approved_contextlist $contextlist) {
        global $DB;

        $contextids = [];
        foreach ($contextlist->get_contexts() as $context) {
            $contextids[] = $context->id;
        }

        if (empty($contextids)) {
            return;
        }

        list($insql, $params) = $DB->get_in_or_equal($contextids, SQL_PARAMS_NAMED);
        $params['usermodified'] = $contextlist->get_user()->id;

        $sql = "SELECT c.contextid, c.id, ls.usermodified
                  FROM {local_selfcohort} ls
            INNER JOIN {cohort} c ON c.id = ls.cohortid
                 WHERE ls.usermodified = :usermodified AND c.contextid " . $insql;

        $records = $DB->get_records_sql($sql, $params);

        $index = 0;
        foreach ($records as $record) {
            $index++;
            $subcontext = [
                get_string('pluginname', 'local_selfcohort'),
                'local_selfcohort',
                $index,
            ];

            $data = (object) [
                'contextid' => $record->contextid,
                'cohortid' => $record->id,
                'usermodified' => $record->usermodified,
            ];

            $context = \context::instance_by_id($record->contextid);
            writer::with_context($context)->export_data($subcontext, $data);
        }

        $params['userid'] = $contextlist->get_user()->id;
        $sql = "SELECT c.contextid, c.id, ls.usermodified, ls.userid
                  FROM {local_selfcohort_confirm} ls
            INNER JOIN {cohort} c ON c.id = ls.cohortid
                 WHERE (ls.usermodified = :usermodified OR ls.userid = :userid) AND c.contextid " . $insql;

        $records = $DB->get_records_sql($sql, $params);

        $index = 0;
        foreach ($records as $record) {
            $index++;
            $subcontext = [
                get_string('pluginname', 'local_selfcohort'),
                'local_selfcohort_confirm',
                $index,
            ];

            $data = (object) [
                'contextid' => $record->contextid,
                'cohortid' => $record->id,
                'usermodified' => $record->usermodified,
                'userid' => $record->userid,
            ];

            $context = \context::instance_by_id($record->contextid);
            writer::with_context($context)->export_data($subcontext, $data);
        }

        $params['userid'] = $contextlist->get_user()->id;
        $sql = "SELECT c.contextid, c.id, lsa.usermodified
                  FROM {local_selfcohort_confirm} ls
            INNER JOIN {local_selfcohort_approve} lsa ON lsa.confirmid = ls.id                                                      
            INNER JOIN {cohort} c ON c.id = ls.cohortid                                
                 WHERE ls.usermodified = :usermodified AND c.contextid " . $insql;

        $records = $DB->get_records_sql($sql, $params);

        $index = 0;
        foreach ($records as $record) {
            $index++;
            $subcontext = [
                get_string('pluginname', 'local_selfcohort'),
                'local_selfcohort_approve',
                $index,
            ];

            $data = (object) [
                'contextid' => $record->contextid,
                'usermodified' => $record->usermodified,
            ];

            $context = \context::instance_by_id($record->contextid);
            writer::with_context($context)->export_data($subcontext, $data);
        }
    }

    /**
     * Delete all data for all users in the specified context.
     *
     * @param \context $context
     */
    public static function delete_data_for_all_users_in_context(\context $context) {
        global $DB;

        $cohorts = $DB->get_records('cohort', ['contextid' => $context->id]);
        foreach ($cohorts as $cohort) {
            // We don't want to delete records. Just anonymise the users.
            $DB->set_field('local_selfcohort', 'usermodified', 0, ['cohortid' => $cohort->id]);
            $DB->delete_records('local_selfcohort_confirm', ['cohortid' => $cohort->id]);

            $sql = "SELECT lsa.*
                      FROM {local_selfcohort_approve} lsa 
                INNER JOIN {local_selfcohort_confirm} ls ON lsa.confirmid = ls.id    
                     WHERE ls.cohortid = :cohortid ";
            $records = $DB->get_records_sql($sql, ['cohortid' => $cohort->id]);
            foreach ($records as $record) {
                $DB->delete_records('local_selfcohort_approve', ['id' => $record->id]);
            }
        }
    }

    /**
     * Delete all user data for the specified user, in the specified contexts.
     *
     * @param \core_privacy\local\request\approved_contextlist $contextlist
     */
    public static function delete_data_for_user(approved_contextlist $contextlist) {
        global $DB;

        list($insql, $params) = $DB->get_in_or_equal($contextlist->get_contextids(), SQL_PARAMS_NAMED);
        $cohorts = $DB->get_records_select('cohort', "contextid $insql", $params);

        foreach ($cohorts as $cohort) {
            $params = [];
            $params['usermodified'] = $contextlist->get_user()->id;
            $params['cohortid'] = $cohort->id;

            // We don't want to delete records. Just anonymise the users.
            $DB->set_field_select(
                'local_selfcohort',
                'usermodified',
                0,
                "usermodified = :usermodified AND cohortid = :cohortid",
                $params
            );

            $params = ['cohortid' => $cohort->id, 'userid' => $contextlist->get_user()->id];
            $DB->delete_records('local_selfcohort_confirm', $params);

            $params = ['cohortid' => $cohort->id, 'usermodified' => $contextlist->get_user()->id];
            $DB->delete_records('local_selfcohort_confirm', $params);

            $sql = "SELECT lsa.*
                      FROM {local_selfcohort_approve} lsa 
                INNER JOIN {local_selfcohort_confirm} ls ON lsa.confirmid = ls.id    
                     WHERE ls.cohortid = :cohortid AND lsa.usermodified = :usermodified";
            $records = $DB->get_records_sql($sql, $params);
            foreach ($records as $record) {
                $DB->delete_records('local_selfcohort_approve', ['id' => $record->id]);
            }

        }
    }

    /**
     * Get the list of users who have data within a context.
     *
     * @param \core_privacy\local\request\userlist $userlist
     */
    public static function get_users_in_context(userlist $userlist) {
        $context = $userlist->get_context();

        $sql = "SELECT ls.usermodified AS userid
                  FROM {local_selfcohort} ls
                  JOIN {cohort} c ON c.id = ls.cohortid 
                 WHERE c.contextid = :contextid";
        $userlist->add_from_sql('userid', $sql, ['contextid' => $context->id]);

        $sql = "SELECT ls.usermodified AS userid
                  FROM {local_selfcohort_confirm} ls
                  JOIN {cohort} c ON c.id = ls.cohortid 
                 WHERE c.contextid = :contextid";
        $userlist->add_from_sql('userid', $sql, ['contextid' => $context->id]);

        $sql = "SELECT ls.userid AS userid
                  FROM {local_selfcohort_confirm} ls
                  JOIN {cohort} c ON c.id = ls.cohortid 
                 WHERE c.contextid = :contextid";
        $userlist->add_from_sql('userid', $sql, ['contextid' => $context->id]);

        $sql = "SELECT lsa.usermodified AS userid
                  FROM {local_selfcohort_confirm} ls
                  JOIN {cohort} c ON c.id = ls.cohortid 
                  JOIN {local_selfcohort_approve} lsa ON lsa.confirmid = ls.id                                                                                       
                 WHERE c.contextid = :contextid";

        $userlist->add_from_sql('userid', $sql, ['contextid' => $context->id]);
    }

    /**
     * Delete multiple users within a single context.
     *
     * @param \core_privacy\local\request\approved_userlist $userlist
     */
    public static function delete_data_for_users(approved_userlist $userlist) {
        global $DB;

        $userids = $userlist->get_userids();

        if (empty($userids)) {
            return;
        }

        list($insql, $inparams) = $DB->get_in_or_equal($userids, SQL_PARAMS_NAMED);

        // We don't want to delete records. Just anonymise the users.
        $DB->set_field_select('local_selfcohort', 'usermodified', 0, "usermodified {$insql}", $inparams);

        $DB->delete_records_select('local_selfcohort_confirm', "userid {$insql}", $inparams);
        $DB->delete_records_select('local_selfcohort_confirm', "usermodified {$insql}", $inparams);
        $DB->delete_records_select('local_selfcohort_approve', "usermodified {$insql}", $inparams);
    }
}
