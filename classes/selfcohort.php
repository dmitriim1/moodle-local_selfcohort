<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_selfcohort;

use core_user\fields;
use moodle_url;
use moodle_exception;
use html_writer;
use local_selfcohort\local\table\cohort_settings;
use local_selfcohort\local\table\confirm;
use context;
use context_system;

/**
 *  Main class
 *
 * @package local_selfcohort
 * @copyright 2016 Davo Smith, Synergy Learning UK on behalf of Alexander Bias, Ulm University <alexander.bias@uni-ulm.de>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class selfcohort {

    /**
     * Action.
     * @var array|false|float|int|mixed|string|null
     */
    protected $action;

    protected $context;

    protected $category = null;

    /** @var string[] the list of available actions */
    protected static $actions = ['cohorts', 'members', 'confirm'];

    /**
     * constructor
     */
    public function __construct() {
        global $PAGE, $DB;

        $this->action = optional_param('action', null, PARAM_ALPHA);
        $contextid = optional_param('contextid', 0, PARAM_INT);

        if ($contextid) {
            $this->context = context::instance_by_id($contextid);
        } else {
            $this->context = context_system::instance();
        }

        if ($this->context->contextlevel != CONTEXT_COURSECAT && $this->context->contextlevel != CONTEXT_SYSTEM) {
            throw new moodle_exception('invalidcontext');
        }

        if ($this->context->contextlevel == CONTEXT_COURSECAT) {
            $this->category = $DB->get_record('course_categories', ['id' => $this->context->instanceid], '*', MUST_EXIST);
        }

        if (!in_array($this->action, static::$actions)) {
            if ($this->can_manage()) {
                $this->action = 'cohorts';
            } else if ($this->can_approve()) {
                $this->action = 'confirm';
            }
        }

        if ($this->category) {
            // Permission check happens in output_form method..
            $url = new moodle_url('/local/selfcohort/cohorts.php');
            $url = new moodle_url($url, ['action' => $this->action, 'contextid' => $this->context->id]);
            $PAGE->set_url($url);
            $PAGE->set_pagelayout('admin');
            $PAGE->set_context($this->context);
            $title = get_string('pluginname', 'local_selfcohort');
            $PAGE->set_title($title);
        } else {
            admin_externalpage_setup('local_selfcohort_config', '', null, $this->get_index_url());
        }
    }

    /**
     * Get the URL of the main page for this plugin.
     * @return \moodle_url
     */
    protected function get_index_url() {
        return new moodle_url('/local/selfcohort/cohorts.php', ['contextid' => $this->context->id]);
    }

    /**
     * Output the complete form for editing profile field mapping rules.
     * @return string
     */
    public function output_form() {
        global $OUTPUT;

        $out = '';

        $tabs = $this->get_tabs();
        $out .= $OUTPUT->render($tabs);

        switch ($this->action) {
            case 'members':
                $this->validate_can_manage();

                $out .= $this->output_members();
                break;
            case 'confirm':
                $this->validate_can_approve();

                $baseurl = $this->get_index_url();
                $baseurl->param('action', $this->action);

                $this->process_confirm($baseurl);

                $out .= html_writer::tag('p', get_string('confirmintro', 'local_selfcohort'), [
                    'id' => 'intro',
                    'class' => 'box generalbox',
                ]);

                $confirmtable = new confirm('local_selfcohort_confirm', $baseurl, $this->context);
                ob_start();
                $confirmtable->out($confirmtable->pagesize, true);
                $out .= ob_get_contents();
                ob_end_clean();
                break;
            default:
                $this->validate_can_manage();
                $registerlink = (new \moodle_url('/local/selfcohort/register.php'))->out();
                $intro = '<p>' . get_string('cohortsintro', 'local_selfcohort') . '</p>' .
                    '<p>' . get_string('invisiblecohortsnote', 'local_selfcohort') . '</p>' .
                    '<p>' . get_string('selfregisterlink', 'local_selfcohort', $registerlink) . '</p>' .
                    '<p>' . get_string('selfregisterintrostring', 'local_selfcohort', $registerlink) . '</p>';
                $out .= html_writer::tag('div', $intro, ['id' => 'intro', 'class' => 'box generalbox']);

                $settingstable = new cohort_settings('local_selfcohort_setings', $this->get_index_url(), $this->context);
                ob_start();
                $settingstable->out($settingstable->pagesize, true);
                $out .= ob_get_contents();
                ob_end_clean();
        }

        return $out;
    }

    /**
     * Process confirmation.
     *
     * @param \moodle_url $redirecturl URL to redirect to after processing.
     */
    private function process_confirm(moodle_url $redirecturl) {
        $userid = optional_param('userid', 0, PARAM_INT);
        $cohortid = optional_param('cohortid', 0, PARAM_INT);
        $confirm = optional_param('confirm', 0, PARAM_INT);
        $roleid = optional_param('roleid', null, PARAM_INT);

        if (!empty($userid) && !empty($cohortid) && confirm_sesskey()) {
            $manager = new membership_manager();
            if ($confirm) {
                $result = $manager->approve_membership_request($cohortid, $userid, $roleid);
            } else {
                $result = $manager->decline_membership_request($cohortid, $userid, $roleid);
            }
            redirect($redirecturl, $result->get_message(), null, $result->get_messagetype());
        }
    }

    /**
     * Allow subclasses to define extra tabs to be included at the top of the page.
     * @return \tabobject
     */
    protected function get_tabs() {
        $tabs = [];

        if ($this->can_manage()) {
            $tabs[] = new \tabobject('members', new \moodle_url('/local/selfcohort/cohorts.php',
                ['action' => 'members', 'contextid' => $this->context->id]
            ), get_string('members', 'local_selfcohort'));

            $tabs[] = new \tabobject('cohorts', new \moodle_url('/local/selfcohort/cohorts.php',
                ['contextid' => $this->context->id]
            ), get_string('selectcohorts', 'local_selfcohort'));
        }

        if ($this->can_approve()) {
            $tabs[] = new \tabobject('confirm', new \moodle_url('/local/selfcohort/cohorts.php',
                ['action' => 'confirm', 'contextid' => $this->context->id]
            ), get_string('confirmrequests', 'local_selfcohort'));
        }

        return new \tabtree($tabs, $this->action);
    }

    /**
     * Output the cohort members list.
     * @return string
     */
    protected function output_members() {
        global $OUTPUT, $DB;

        $out = \html_writer::tag('div', get_string('membersintro', 'local_selfcohort').'<br/>'.
                                 get_string('invisiblecohortsnote', 'local_selfcohort'),
                                 array('id' => 'intro', 'class' => 'box generalbox'));

        $namefields = implode(',', fields::get_name_fields());

        $contextsql = '';
        $params = [];
        if ($this->context->contextlevel != CONTEXT_SYSTEM) {
            $contextsql = 'AND  c.contextid = ? ';
            $params = [$this->context->id];
        }

        $sql = "
          SELECT c.id AS cohortid, c.name AS cohortname, u.id, {$namefields}
            FROM {cohort} c
            LEFT JOIN {cohort_members} cm ON cm.cohortid = c.id
            LEFT JOIN {user} u ON u.id = cm.userid
           WHERE c.component = 'local_selfcohort' $contextsql
           ORDER BY c.name, c.id, u.lastname, u.firstname";
        $users = $DB->get_recordset_sql($sql, $params);

        $lastcohortid = null;
        $lastcohortname = null;
        $list = '';
        $cohortmembers = [];
        foreach ($users as $user) {
            if ($user->cohortid != $lastcohortid) {
                if ($lastcohortid) {
                    $list .= $this->output_members_entry($lastcohortname, $cohortmembers);
                }
                $cohortmembers = [];
                $lastcohortid = $user->cohortid;
                $lastcohortname = $user->cohortname;
            }
            if ($user->id) {
                $userurl = new \moodle_url('/user/view.php', ['id' => $user->id]);
                $cohortmembers[] = html_writer::link($userurl, fullname($user));
            }
        }
        if ($lastcohortid) {
            $list .= $this->output_members_entry($lastcohortname, $cohortmembers);
        }

        $out .= html_writer::div($list, '', ['id' => 'profilecohort-cohortlist', 'role' => 'tablist',
                                             'aria-multiselectable' => 'true']);

        return $out;
    }

    /**
     * Render a cohortlist entry for output_members().
     * @param string $cohortname
     * @param string[] $cohortmembers
     * @return string
     */
    private function output_members_entry($cohortname, $cohortmembers) {
        $out = '';

        // Create HTML element ID from cohortname.
        $id = 'profilecohort-cohortlist-'.preg_replace('/\W+/', '', strtolower($cohortname));

        // Bootstrap collapse header.
        $out .= html_writer::start_div('card-header', ['id' => $id.'-heading', 'role' => 'tab']);
        $out .= html_writer::link('#'.$id, format_string($cohortname),
                ['class' => 'collapsed', 'data-toggle' => 'collapse', 'data-parent' => '#profilecohort-cohortlist',
                 'aria-expanded' => 'false', 'aria-controls' => $id]);
        $out .= html_writer::end_div();

        // Bootstrap collapse content.
        if ($cohortmembers) {
            $content = '';
            foreach ($cohortmembers as $cohortmember) {
                $content .= html_writer::tag('li', $cohortmember);
            }
            $content = html_writer::tag('ul', $content);
        } else {
            $content = get_string('nousers', 'local_selfcohort');
        }
        $out .= html_writer::start_div('collapse', ['id' => $id, 'role' => 'tabpanel', 'aria-labelledby' => $id.'-heading']);
        $out .= html_writer::div($content, 'card-body');
        $out .= html_writer::end_div();

        return html_writer::div($out, 'card');
    }

    /**
     * Check if can manage cohorts.
     *
     * @return bool
     */
    private function can_manage(): bool {
        return capability_manager::can_manage($this->context);
    }

    /**
     * Check if can approve membership requests.
     *
     * @return bool
     */
    private function can_approve(): bool {
        return capability_manager::can_approve_membership($this->context);
    }

    /**
     * Validates manage permissions.
     *
     * @return void
     */
    private function validate_can_manage(): void {
        capability_manager::validate_can_manage($this->context);
    }

    /**
     * Validates approval permissions.
     *
     * @return void
     */
    private function validate_can_approve(): void {
        capability_manager::validate_can_approve_membership($this->context);
    }
}
