<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

namespace local_selfcohort;

use core\event\cohort_deleted;
use Exception;

/**
 * Event observer class.
 *
 * @package    local_selfcohort
 * @author     Dmitrii Metelkin <dmitriim@catalyst-au.net>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class observer {

    /**
     * Handle group_deleted event.
     *
     * @param \core\event\cohort_deleted $event
     */
    public static function cohort_deleted(cohort_deleted $event) {
        try {
            $record = cohort_settings::get_record(['cohortid' => $event->objectid]);
            if ($record) {
                $record->delete();
            }
        } catch (Exception $exception) {
            debugging($exception->getMessage(), DEBUG_DEVELOPER);
        }
    }
}
