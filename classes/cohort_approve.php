<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

namespace local_selfcohort;

use core\persistent;

/**
 * Cohort approval records.
 *
 * @package    local_selfcohort
 * @author     Dmitrii Metelkin <dmitriim@catalyst-au.net>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class cohort_approve extends persistent {

    /**
     * DB table.
     */
    public const TABLE = 'local_selfcohort_approve';

    /**
     * New status.
     */
    public const STATUS_NEW = 0;

    /**
     * Approved status.
     */
    public const STATUS_APPROVED = 1;

    /**
     * Declined status.
     */
    public const STATUS_DECLINED = 2;

    /**
     * Defines properties.
     * @return array
     */
    protected static function define_properties(): array {
        return [
            'confirmid' => [
                'type' => PARAM_INT,
            ],
            'roleid' => [
                'type' => PARAM_INT,
                'default' => 0,
            ],
            'status' => [
                'type' => PARAM_INT,
                'default' => 0,
                'choices' => [
                    self::STATUS_NEW,
                    self::STATUS_APPROVED,
                    self::STATUS_DECLINED,
                ],
            ],
        ];
    }

    /**
     * Delete all records.
     */
    public static function purge_all(): void {
        foreach (self::get_records() as $record) {
            $record->delete();
        }
    }

    /**
     * Delete all records for a given confirmation.
     */
    public static function purge_for_confirmation(int $confirmid): void {
        foreach (self::get_records(['confirmid' => $confirmid]) as $record) {
            $record->delete();
        }
    }
}
