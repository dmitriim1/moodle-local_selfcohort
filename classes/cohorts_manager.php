<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_selfcohort;

use stdClass;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/cohort/lib.php');

/**
 * Class responsible for managing cohorts.
 *
 * Please note: this class doesn't validate any permissions.
 *
 * @package    local_selfcohort
 * @author     Dmitrii Metelkin <dmitriim@catalyst-au.net>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class cohorts_manager {

    /**
     * A cached list of cohort settings.
     *
     * @var cohort_settings[]
     */
    protected $cohortsettings = [];

    /**
     * A list of cohort managed by a plugin.
     *
     * @var array
     */
    protected $managedcohorts = [];

    /**
     * Cached list of count of memebers for each cohort.
     *
     * @var array
     */
    protected $totalmembers = [];

    /**
     * A list of cohort memberships keys by userid.
     * @var array
     */
    protected $memberships = [];

    /**
     * Is multiple memberships allowed?
     * @var bool
     */
    protected $allowmultiple;

    /**
     * Constructor.
     */
    public function __construct() {
        global $DB;

        $this->allowmultiple = get_config('local_selfcohort', 'selectmany');

        foreach (cohort_settings::get_records() as $setting) {
            $this->cohortsettings[$setting->get('cohortid')] = $setting;
        }

        $this->totalmembers = $DB->get_records_sql('SELECT cohortid, COUNT(*) AS count FROM {cohort_members} GROUP BY cohortid');
        $this->managedcohorts = $DB->get_records('cohort', ['component' => 'local_selfcohort']);
    }

    /**
     * Start managing cohort.
     *
     * @param int $cohortid Cohort ID.
     *
     * @return \stdClass Cohort object.
     */
    public function manage_cohort(int $cohortid) : stdClass {
        global $DB;

        $DB->set_field('cohort', 'component', 'local_selfcohort', ['id' => $cohortid]);
        $this->managedcohorts[$cohortid] = $DB->get_record('cohort', ['id' => $cohortid]);
        $this->memberships = [];

        return $this->managedcohorts[$cohortid];
    }

    /**
     * Stop managing cohort.
     *
     * @param int $cohortid Cohort ID.
     *
     * @return \stdClass Cohort object.
     */
    public function unmanage_cohort(int $cohortid) : stdClass {
        global $DB;

        $DB->set_field('cohort', 'component', '', ['id' => $cohortid]);

        if (isset($this->managedcohorts[$cohortid])) {
            unset($this->managedcohorts[$cohortid]);
            $this->memberships = [];
        }

        $confirms = cohort_confirm::get_records(['cohortid' => $cohortid]);
        foreach ($confirms as $confirm) {
            $confirm->delete();
        }

        return $DB->get_record('cohort', ['id' => $cohortid]);
    }

    /**
     * Check if a given user is a member of given cohort.
     * This will check memberships only for managed cohorts.
     *
     * @param int $cohortid Cohort ID.
     * @param int $userid User ID.
     *
     * @return bool
     */
    public function is_member(int $cohortid, int $userid) : bool {
        return !empty($this->get_memberships($userid)[$cohortid]);
    }

    /**
     * Returns a list of cohorts a user is a member of.
     *
     * @param int $userid
     *
     * @return array
     */
    protected function get_memberships(int $userid): array {
        global $DB;

        if (!isset($this->memberships[$userid])) {
            $this->memberships[$userid] = [];

            if ($this->managedcohorts) {
                list($cohortssql, $params) = $DB->get_in_or_equal(array_keys($this->managedcohorts), SQL_PARAMS_NAMED);
                $sql = "SELECT cohortid FROM {cohort_members} WHERE userid = :userid and cohortid $cohortssql";
                $params['userid'] = $userid;
                $this->memberships[$userid] = $DB->get_records_sql($sql, $params);
            }
        }

        return $this->memberships[$userid];
    }

    /**
     * Return a maximum members allowed for a given cohort.
     *
     * @param int $cohortid Cohort ID.
     *
     * @return int
     */
    public function get_max_members(int $cohortid): int {
        if (isset($this->cohortsettings[$cohortid])) {
            return $this->cohortsettings[$cohortid]->get('maxmembers');
        } else {
            return 0;
        }
    }

    /**
     * Returns a total members for a given cohort.
     *
     * @param int $cohortid Cohort ID.
     *
     * @return int
     */
    public function get_total_members(int $cohortid): int {
        if (isset($this->totalmembers[$cohortid])) {
            return $this->totalmembers[$cohortid]->count;
        } else {
            return 0;
        }
    }

    /**
     * Check if a given user can be added to a given cohort.
     *
     * @param int $cohortid Cohort ID.
     * @param int $userid User ID.
     *
     * @return bool
     */
    public function can_add_to_cohort(int $cohortid, int $userid): bool {
        if (!key_exists($cohortid, $this->managedcohorts)) {
            return false;
        }

        if (!$this->allowmultiple) {
            return empty($this->get_memberships($userid));
        }

        return true;
    }

    /**
     * Check if a given cohort is full.
     *
     * @param int $cohortid Cohort ID.
     *
     * @return bool
     */
    public function is_cohort_full(int $cohortid): bool {
        if ($this->get_max_members($cohortid) < 0) {
            return true;
        }

        if ($this->get_max_members($cohortid) > 0 && $this->get_total_members($cohortid) >= $this->get_max_members($cohortid)) {
              return true;
        }

        return false;
    }

    /**
     * Check if provided cohort requires admin confirmation before joining.
     *
     * @param int $cohortid Cohort ID.
     *
     * @return bool
     */
    public function require_confirm(int $cohortid): bool {
        if (isset($this->cohortsettings[$cohortid])) {
            return (bool) $this->cohortsettings[$cohortid]->get('confirm');
        } else {
            return false;
        }
    }

    /**
     * Add a given user to a cohort.
     *
     * @param int $cohortid Cohort ID,
     * @param int $userid User ID.
     *
     * @return action_result
     */
    public function add_to_cohort(int $cohortid, int $userid): action_result {
        if (!key_exists($cohortid, $this->managedcohorts)) {
            return new action_result(false, get_string('invalidcohort', 'local_selfcohort'));
        }

        if ($this->is_cohort_full($cohortid)) {
            return new action_result(false, get_string('cohortfull', 'local_selfcohort'));
        }

        cohort_add_member($cohortid, $userid);

        return new action_result(true, get_string('addedtocohort', 'local_selfcohort'));
    }

    /**
     * Remove a given user from a given cohort.
     *
     * @param int $cohortid Cohort ID.
     * @param int $userid User ID.
     *
     * @return action_result
     */
    public function remove_from_cohort(int $cohortid, int $userid): action_result {
        if (!key_exists($cohortid, $this->managedcohorts)) {
            return new action_result(false, get_string('invalidcohort', 'local_selfcohort'));
        }

        cohort_remove_member($cohortid, $userid);
        return new action_result(true, get_string('removedfromcohort', 'local_selfcohort'));
    }

    /**
     * Check if we manage a given cohort.
     *
     * @param int $cohortid Cohort ID.
     *
     * @return bool
     */
    public function is_managed_cohort(int $cohortid): bool {
        return key_exists($cohortid, $this->managedcohorts);
    }
}
