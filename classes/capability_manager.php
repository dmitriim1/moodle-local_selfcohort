<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

namespace local_selfcohort;

use context;

/**
 * Capability manager class responsible for checking capabilities.
 *
 * @package    local_selfcohort
 * @author     Dmitrii Metelkin <dmitriim@catalyst-au.net>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class capability_manager {

    /**
     * Capability required for managing cohorts.
     */
    public const CAPABILITY_MANAGE = 'moodle/cohort:manage';

    /**
     * Capability required for approving.
     */
    public const CAPABILITY_RECEIVE_NOTIFICATION = 'local/selfcohort:emailrequestmembership';

    /**
     * Capability required for approving.
     */
    public const CAPABILITY_APPROVE = 'local/selfcohort:approverequestmembership';

    /**
     * Check if can manage cohorts in a given context.
     *
     * @param \context $context
     * @return bool
     */
    public static function can_manage(context $context): bool {
        return has_capability(self::CAPABILITY_MANAGE, $context);
    }

    /**
     * Check if can approve cohort membership requests in a given context.
     *
     * @param \context $context
     * @return bool
     */
    public static function can_approve_membership(context $context): bool {
        return has_capability(self::CAPABILITY_APPROVE, $context);
    }

    /**
     * Validates manage permissions in a given context.
     *
     * @param \context $context
     */
    public static function validate_can_manage(context $context): void {
        require_capability(self::CAPABILITY_MANAGE, $context);
    }

    /**
     * Validates manage permissions in a given context.
     *
     * @param \context $context
     */
    public static function validate_can_approve_membership(context $context): void {
        require_capability(self::CAPABILITY_APPROVE, $context);
    }
}
