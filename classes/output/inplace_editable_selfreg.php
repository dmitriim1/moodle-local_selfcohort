<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

namespace local_selfcohort\output;

use core\output\inplace_editable;

/**
 * In place editable for toggling self registration status.
 *
 * @package    local_selfcohort
 * @author     Dmitrii Metelkin <dmitriim@catalyst-au.net>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class inplace_editable_selfreg extends inplace_editable {

    /**
     * Constructor.
     *
     * @param \stdClass $cohort Cohort instance.
     */
    public function __construct(\stdClass $cohort) {
        global $OUTPUT;

        $icon = $cohort->component == 'local_selfcohort' ? 'i/checked' : 'i/unchecked';
        $alt = $cohort->component == 'local_selfcohort' ? get_string('disable') : get_string('enable');
        $value = $cohort->component == 'local_selfcohort' ? 0 : 1;

        parent::__construct(
            'local_selfcohort',
            'selfreg',
            $cohort->id,
            true,
            $OUTPUT->pix_icon($icon, $alt, 'moodle', ['title' => $alt]),
            $value
        );

        $this->set_type_toggle([1, 0]);
    }
}
