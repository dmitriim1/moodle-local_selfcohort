<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

namespace local_selfcohort\output;

use core\output\inplace_editable;
use local_selfcohort\cohort_settings;

/**
 * In place editable for confirmation  value.
 *
 * @package    local_selfcohort
 * @author     Dmitrii Metelkin <dmitriim@catalyst-au.net>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class inplace_editable_confirm extends inplace_editable {

    /**
     * Constructor.
     *
     * @param cohort_settings $cohortsettings Instance of cohort settings.
     */
    public function __construct(cohort_settings $cohortsettings) {
        global $OUTPUT;

        $value = (int) $cohortsettings->get('confirm');

        $icon = !empty($value) ? 'i/checked' : 'i/unchecked';
        $alt = !empty($value) ? get_string('disable') : get_string('enable');

        parent::__construct(
            'local_selfcohort',
            'confirm',
            $cohortsettings->get('id'),
            true,
            $OUTPUT->pix_icon($icon, $alt, 'moodle', ['title' => $alt]),
            $value
        );

        $this->set_type_toggle([0, 1]);
    }

}
