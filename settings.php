<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * This adds the custom fields management page.
 *
 * @package     local_selfcohort
 * @copyright   2020 Daniel Neis Araujo <daniel@adapta.online>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use local_selfcohort\admin_setting_configmulticheckbox_custom;
use local_selfcohort\admin_setting_configselect_custom;
use local_selfcohort\capability_manager;
use local_selfcohort\membership_manager;

defined('MOODLE_INTERNAL') || die;

$ADMIN->add('accounts',
    new admin_externalpage('local_selfcohort_config', new lang_string('pluginname', 'local_selfcohort'),
        new moodle_url('/local/selfcohort/cohorts.php'), ['moodle/cohort:manage', 'moodle/cohort:view']
    )
);

if ($hassiteconfig) {
    $settings = new admin_settingpage('local_selfcohort', new lang_string('pluginname', 'local_selfcohort'));
    $settings->add(new admin_setting_configcheckbox('local_selfcohort/selectmany',
        new lang_string('selectmany', 'local_selfcohort'), new lang_string('selectmanydesc', 'local_selfcohort'), '1'
        )
    );

    $settings->add(new admin_setting_configcheckbox('local_selfcohort/notifyadmins',
            new lang_string('notifyadmins', 'local_selfcohort'), new lang_string('notifyadminsdesc', 'local_selfcohort'), '0'
        )
    );

    $enableroleapproval = get_config('local_selfcohort', 'enableroleapproval');

    $capableroles = get_roles_with_capability(capability_manager::CAPABILITY_APPROVE, CAP_ALLOW);
    $roles = [];
    foreach ($capableroles as $key => $role) {
        $roles[$key] = role_get_name($role);
    }

    $warning = '';
    if (empty($roles)) {
        $warning = $OUTPUT->notification(get_string('warningnoroles', 'local_selfcohort'), 'warning', false);
    } else if ($enableroleapproval && empty(get_config('local_selfcohort', 'approvalroles'))) {
        $warning = $OUTPUT->notification(get_string('warningnnorolesconfigured', 'local_selfcohort'), 'warning', false);
    }

    $yesnooptions = [
        0 => get_string('no'),
        1 => get_string('yes'),
    ];

    $info = $OUTPUT->notification(get_string('resetinfo', 'local_selfcohort'), 'info', false);
    $settings->add(new admin_setting_heading('local_selfcohort/resetinfo', '', $info));

    $settings->add(new admin_setting_configselect_custom('local_selfcohort/enableroleapproval',
            new lang_string('enableroleapproval', 'local_selfcohort'),
            new lang_string('enableroleapprovaldesc', 'local_selfcohort') . $warning,
            0,
            $yesnooptions)
    );

    if ($enableroleapproval && !empty($roles)) {
        $settings->add(new admin_setting_configmulticheckbox_custom('local_selfcohort/approvalroles',
                new lang_string('approvalroles', 'local_selfcohort'),
                new lang_string('approvalrolesdesc', 'local_selfcohort'), [], $roles)
        );

        $rules = [
            membership_manager::APPROVAL_RULE_ALL => get_string('approvalrule_all', 'local_selfcohort'),
            membership_manager::APPROVAL_RULE_ANY => get_string('approvalrule_any', 'local_selfcohort'),
        ];

        $settings->add(new admin_setting_configselect_custom('local_selfcohort/approvalrule',
                new lang_string('approvalrule', 'local_selfcohort'),
                new lang_string('approvalruledesc', 'local_selfcohort'), membership_manager::APPROVAL_RULE_ANY, $rules)
        );

        $settings->add(new admin_setting_configselect_custom('local_selfcohort/adminfinalapprove',
                new lang_string('adminfinalapprove', 'local_selfcohort'),
                new lang_string('adminfinalapprovedesc', 'local_selfcohort'),
                0,
                $yesnooptions)
        );
    }

    $ADMIN->add('localplugins', $settings);
}
