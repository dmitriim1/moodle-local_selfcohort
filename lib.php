<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Lib callbacks.
 *
 * @package    local_selfcohort
 * @author     Dmitrii Metelkin <dmitriim@catalyst-au.net>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use core\output\inplace_editable;
use local_selfcohort\cohorts_manager;
use local_selfcohort\output\inplace_editable_maxmembers;
use local_selfcohort\output\inplace_editable_selfreg;
use local_selfcohort\output\inplace_editable_confirm;
use local_selfcohort\cohort_settings;
use local_selfcohort\capability_manager;

/**
 * Manage inplace editable saves.
 *
 * @param string $itemtype The type of item.
 * @param int $itemid The ID of the item.
 * @param mixed $newvalue The new value
 *
 * @return ? inplace_editable
 */
function local_selfcohort_inplace_editable(string $itemtype, int $itemid, $newvalue): ?inplace_editable {
    global $DB;

    switch ($itemtype) {
        case 'maxmembers':
            $settings = cohort_settings::get_record(['id' => $itemid]);
            local_selfcohort_validate_permissions_inplace_editable($settings->get('cohortid'));
            $newvalue = clean_param($newvalue, PARAM_INT);
            $settings->set('maxmembers', $newvalue);
            $settings->save();
            return new inplace_editable_maxmembers($settings);

        case 'selfreg':
            local_selfcohort_validate_permissions_inplace_editable($itemid);
            $manager = new cohorts_manager();
            if (empty($newvalue)) {
                $cohort = $manager->unmanage_cohort($itemid);
            } else {
                $cohort = $manager->manage_cohort($itemid);
            }
            return new inplace_editable_selfreg($cohort);

        case 'confirm':
            $settings = cohort_settings::get_record(['id' => $itemid]);
            local_selfcohort_validate_permissions_inplace_editable($settings->get('cohortid'));
            $newvalue = clean_param($newvalue, PARAM_INT);
            $settings->set('confirm', $newvalue);
            $settings->save();
            return new inplace_editable_confirm($settings);

        default:
            throw new coding_exception('Invalid in place editable item type ' . $itemtype);
    }
}

/**
 * Validate permission for in place editable.
 *
 * @param int $cohortid Cohort to validate permissions on.
 *
 * @return void
 */
function local_selfcohort_validate_permissions_inplace_editable(int $cohortid): void {
    global $DB;

    $cohort = $DB->get_record('cohort', ['id' => $cohortid], '*', MUST_EXIST);
    $cohortcontext = context::instance_by_id($cohort->contextid);
    external_api::validate_context($cohortcontext);
    capability_manager::validate_can_manage($cohortcontext);
}

/**
 * Adds a link to the category admin menu.
 *
 * @param navigation_node $navigation The navigation node to extend
 * @param context $context The context of the course
 *
 * @return void
 */
function local_selfcohort_extend_navigation_category_settings(navigation_node $navigation, context $context): void {
    if (capability_manager::can_manage($context) || capability_manager::can_approve_membership($context)) {
        $navigation->add(
            get_string('pluginname', 'local_selfcohort'),
            new moodle_url('/local/selfcohort/cohorts.php', ['contextid' => $context->id]),
        );
    }
}
