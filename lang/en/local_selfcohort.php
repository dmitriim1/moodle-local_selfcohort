<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Plugin strings are defined here.
 *
 * @package     local_selfcohort
 * @category    string
 * @copyright   2020 Daniel Neis Araujo <daniel@adapta.online>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addme'] = 'Add me to cohort';
$string['addedtocohort'] = 'You were successfully added to the selected cohort';
$string['adminfinalapprove'] = 'Admin must approve';
$string['adminfinalapprovedesc'] = 'If enabled, administrator must approve membership requests before users can be added to cohorts.';
$string['approval'] = 'Approval';
$string['approved'] = 'Approved';
$string['approvalroles'] = 'Roles for approval';
$string['approvalrolesdesc'] = 'Select roles that must approve cohort membership requests. Note: the capability local/selfcohort:approverequestmembership must be allowed for a role to appear in the list.';
$string['approvalrule'] = 'Approval requires';
$string['approvalruledesc'] = '';
$string['approvalrule_all'] = 'ALL selected roles to approve';
$string['approvalrule_any'] = 'ANY selected roles to approve';
$string['availablecohorts'] = 'Cohorts with self registering';
$string['availablecohortsregister'] = 'Select which cohorts you would like to be part of:';
$string['canselectonecohort'] = 'You can select only one cohort';
$string['cohortsintro'] = 'On this tab, you select the cohorts you want this plugin to manage. Once selected, you will not be able to manually update the members of these cohorts anymore. Furthermore, any users who are currently a member of these cohorts will be removed from the cohorts and the cohorts are then filled from scratch with the users matching the rule(s) you create with this plugin.<br>
If you decide to stop managing a cohort with this plugin and deselect it here, all users who are currently a member of this cohort will keep being a member. Additionally, you will be able to manually update the members of this cohort again.';
$string['cohort'] = 'Cohort';
$string['cohortfull'] = 'Cohort is full';
$string['confirmpending'] = 'Membership pending approval';
$string['confirmrequested'] = 'Your membership request has been saved successfully. You will be sent a notification to inform you whether your request was approved.';
$string['confirmapproved'] = 'Membership request has been approved successfully. Notification has been sent to a user.';
$string['confirmapprovedbyrole'] = 'Membership request has been approved by selected role. All configured roles need to approve the request before a user can be added to a cohort.';
$string['confirmdeclined'] = 'Membership request has been declined successfully. Notification has been sent to a user.';
$string['confirmrequests'] = 'Cohort membership requests';
$string['confirmintro'] = 'On this tab, you can approve or decline cohort membership requests.';
$string['confirmapprove'] = 'Are you sure you would like to approve this membership request?';
$string['confirmdecline'] = 'Are you sure you would like to decline this membership request?';
$string['decline'] = 'Decline';
$string['enableroleapproval'] = 'Enable per role membership approval';
$string['enableroleapprovaldesc'] = 'If enabled, then following settings will be applied for cohort membership approval process. If disabled, then anyone with the capability local/selfcohort:approverequestmembership will be able to approve.';
$string['event:membership_requested'] = 'Cohort membership requested';
$string['event:membership_request_approved'] = 'Cohort membership request approved';
$string['event:membership_request_approved_by_role'] = 'Cohort membership request approved by role';
$string['event:membership_request_declined'] = 'Cohort membership request declined';
$string['incorrectaction'] = 'Incorrect action';
$string['invalidcohort'] = 'Invalid cohort';
$string['invalidconfirm'] = 'Invalid membership request';
$string['invalidapproval'] = 'Invalid approval request';
$string['invisiblecohortsnote'] = 'Please note: This plugin handles invisible cohorts completely equal to visible cohorts. Thus, the list of cohorts also includes cohorts which have been created as invisible.';
$string['maxmembers'] = 'Max members';
$string['members'] = 'Members';
$string['memberq'] = 'Member?';
$string['membersintro'] = 'On this tab, you can see the users who are currently members of the cohorts which are managed by this plugin.';
$string['messageprovider:confirm'] = 'Confirmation of your cohort membership request';
$string['messageprovider:request'] = 'Notification of cohort membership request';
$string['nocohorts'] = 'No cohorts available at the moment.';
$string['notifyadmins'] = 'Site admins receive membership request notifications';
$string['notifyadminsdesc'] = 'If enabled, site administrators will receive notifications about new membership requests.';
$string['notyetapproved'] = 'Not approved yet';
$string['nousers'] = 'This cohort does not, currently, contain any users';
$string['norequests'] = 'No membership requests at the moment';
$string['pluginname'] = 'Self cohort';
$string['privacy:metadata:local_selfcohort'] = 'Details of self cohort plugin';
$string['privacy:metadata:local_selfcohort:cohortid'] = 'Cohort ID';
$string['privacy:metadata:local_selfcohort:usermodified'] = 'ID of user who last created or modified data.';
$string['privacy:metadata:local_selfcohort_confirm'] = 'Cohort membership requests';
$string['privacy:metadata:local_selfcohort_confirm:cohortid'] = 'Cohort ID';
$string['privacy:metadata:local_selfcohort_confirm:userid'] = 'ID of user who requested a membership.';
$string['privacy:metadata:local_selfcohort_confirm:usermodified'] = 'ID of user who last created or modified data.';
$string['privacy:metadata:local_selfcohort_approve'] = 'Details of self cohort plugin';
$string['privacy:metadata:local_selfcohort_approve:usermodified'] = 'ID of user who last created or modified data.';
$string['description'] = 'Allow users with permissions limit activities for specified groups.';
$string['registerintro'] = 'In this page you can select the cohorts you want to make part of.';
$string['removedfromcohort'] = 'You were successfully removed from the selected cohort';
$string['removeme'] = 'Remove me from cohort';
$string['requiresconfirm'] = 'Require confirmation';
$string['requestconfirm'] = 'Request membership';
$string['requestsubject'] = 'New cohort membership request';
$string['requestfullmessage'] = 'A new membership request from {$a->userfullname} to {$a->cohortname}.';
$string['requestsubjectapprove'] = 'Your cohort membership request has been approved';
$string['requestfullmessageapprove'] = 'Your membership request to cohort {$a->cohortname} has been approved.';
$string['requestsubjectdecline'] = 'Your cohort membership request has been declined';
$string['requestfullmessagedecline'] = 'Your membership request to cohort {$a->cohortname} has been declined.';
$string['selectacohort'] = 'Select a cohort';
$string['selectcohorts'] = 'Select cohorts to be managed';
$string['selectmany'] = 'Allow selecting multiple cohorts';
$string['selectmanydesc'] = 'Enable this option to allow users to select multiple cohorts.';
$string['selfcohort:approverequestmembership'] = 'Approve membership requests';
$string['selfcohort:emailrequestmembership'] = 'Receive new membership requests notifications';
$string['selfreg'] = 'Self registration';
$string['selfregisterlink'] = 'The link to self register in cohorts is: <a href="{$a}">{$a}</a>';
$string['selfregisterintrostring'] = 'To edit the message displayed to users, edit the string with the identifier \'registerintro\' of this module (local_selfcohort).';
$string['warningnoroles'] = 'Per role cohort membership approval won\'t be enabled. Please assign the capability local/selfcohort:approverequestmembership to at least one role.';
$string['warningnnorolesconfigured'] = 'Per role cohort membership approval won\'t be enabled. Please select at least one role for approval.';
$string['resetinfo'] = 'Please note that changing any of the below settings will purge all existing cohort confirmation requests.';
$string['errordeclining'] = 'Error while declining a confirmation request';
